class Occam
  require 'json'

  class Git
    attr_reader :path
    attr_reader :revision

    def initialize(path, revision="HEAD")
      require 'tmpdir'
      require 'fileutils'
      require 'open3'

      @revision = revision
      @path = path
      @tmp  = false

      # Is this a URL?
      if path.match /^[a-z]+:\/\//
        tmpdir = Dir.mktmpdir("occam-")
        @tmp = true
        @path = tmpdir

        # Clone git repository
        Open3.popen3('git', 'clone', path, '.', :chdir => tmpdir) do |i, o, e, t|
        end

        # Define destructor
        ObjectSpace.define_finalizer(self, proc do
          FileUtils.remove_entry_secure tmpdir
        end)
      end

      if @revision == "HEAD"
        @revision = self.head
      end
    end

    def self.fullRevision(path, revision="HEAD")
      Open3.popen3('git', 'rev-parse', revision, :chdir => path) do |i, o, e, t|
        ret = o.read.strip

        errorCode = t.value
        if errorCode != 0
          return nil
        end

        return ret
      end
    end

    def self.hasRevision(path, revision="HEAD")
      Git.fullRevision(path, revision) != false
    end

    def head()
      Git.fullRevision(@path, "HEAD")
    end

    def parent(revision="HEAD")
      Git.fullRevision(@path, revision + "^")
    end

    def children(revision="HEAD")
      revision = Git.fullRevision(@path, revision)

      Open3.popen3('git', 'rev-list', '--all', '--parents', "^" + revision, :chdir => @path) do |i, o, e, t|
        ret = []

        hashes = o.read.lines.last
        if hashes
          hashes = hashes.split(' ').first
          ret << hashes
        end

        errorCode = t.value
        if errorCode != 0
          return []
        end

        return ret
      end
    end

    def retrieveJSON(filepath)
      Open3.popen2('git', 'show', "#{@revision}:#{sanitizePath(filepath)}", :chdir => @path) do |i, o, t|
        i.close

        begin
          ret = JSON.parse(o.read)
        rescue
          ret = {}
        end

        errorCode = t.value
        if errorCode != 0
          return {}
        end

        return ret
      end

      {}
    end

    def retrieveFileInfo(filepath)
      Open3.popen2('git', 'ls-tree', "#{@revision}", '-l', '--', sanitizePath(filepath), :chdir => @path) do |i, o, t|
        i.close

        ret = o.read

        errorCode = t.value
        if errorCode != 0
          return {}
        end

        ret.lines do |line|
          info = {}
          components = line.split(' ').map(&:strip)
          info[:stat] = components[0]
          info[:type] = components[1]
          info[:hash] = components[2]
          if components[3] == "-"
            info[:size] = ""
          else
            begin
              info[:size] = components[3].to_i
            rescue
              info[:size] = 0
            end
          end
          info[:name] = components[4..-1].join(' ')

          return info
        end
      end
    end

    def eachFile(filepath="", filter=nil, &block)
      # git ls-tree <revision>:<path> -l
      # Yields the directory listing for that revision. -l gives us the file size.
      # <stat octal> <type> <hash> <size or '-'> <name>
      Open3.popen2('git', 'ls-tree', "#{@revision}:#{sanitizePath(filepath)}", '-l', :chdir => @path) do |i, o, t|
        i.close

        ret = o.read

        errorCode = t.value
        if errorCode != 0
          return false
        end

        ret.lines do |line|
          info = {}
          components = line.split(' ').map(&:strip)
          info[:stat] = components[0]
          info[:type] = components[1]
          info[:hash] = components[2]
          if components[3] == "-"
            info[:size] = ""
          else
            begin
              info[:size] = components[3].to_i
            rescue
              info[:size] = 0
            end
          end
          info[:name] = components[4..-1].join(' ')

          next if filter && filter == :trees && info[:type] != "tree"
          next if filter && filter == :files && info[:type] == "tree"

          yield(info)
        end
      end
    end

    def sanitizePath(filepath)
      filepath
    end

    def retrieveFile(filepath)
      Open3.popen2('git', 'show', "#{@revision}:#{sanitizePath(filepath)}", :chdir => @path) do |i, o, t|
        i.close

        ret = o.read

        errorCode = t.value
        if errorCode != 0
          return false
        end

        return ret
      end

      ""
    end

    def objectInfo
      self.retrieveJSON('object.json')
    end

    def name
      self.objectInfo['name'] || ""
    end

    def type
      self.objectInfo['type'] || ""
    end

    def authors
      self.objectInfo['authors'] || []
    end

    def description
      self.objectInfo['description'] || ""
    end

    def configurations
      self.objectInfo['configurations'] || []
    end

    def outputs
      self.objectInfo['outputs'] || []
    end

    def inputs
      self.objectInfo['inputs'] || []
    end

    def installInfo
      ret = self.objectInfo['install'] || []

      if not ret.is_a? Array
        ret = [ret]
      end

      ret
    end

    def buildInfo
      self.objectInfo['build'] || {}
    end

    def runInfo
      self.objectInfo['run'] || {}
    end

    def commits(forPath = nil, limit = 10)
      # An empty path argument is allowed
      forPath ||= ""
      limit   ||= 10

      ret = []

      Open3.popen2('git', 'log', "-#{limit}", '--', forPath, :chdir => @path) do |i, o, t|
        i.close

        data = o.read

        commit = nil

        readingCommit = false

        data.lines.each do |line|
          line = line.chomp

          commit ||= {:message  => "",
                      :author   => "",
                      :date     => Date.today,
                      :revision => "HEAD"}

          if readingCommit
            if line == ""
              readingCommit = false
              ret << commit
              commit = nil
            else
              commit[:message] << line << "\n"
            end
          else
            if line.start_with? "commit "
              commit[:revision] = line[7..-1].strip
            elsif line.start_with? "Author: "
              commit[:author] = line[8..-1].strip
            elsif line.start_with? "Date: "
              commit[:date] = line[6..-1].strip
            end

            if line == ""
              readingCommit = true
            end
          end
        end

        errorCode = t.value
        if errorCode != 0
          return false
        end

        return ret
      end
    end

    # Read the OCCAM Object description JSON from the given
    # git repository. Returns a Hash of the description data
    # found there.
    def self.description(path)
      Occam::Git.import_scripts(path) do |dir|
        begin
          description = JSON.parse(IO.read("#{dir}/object.json"))
        rescue
          description = {}
        end

        description
      end
    end

    def self.import_scripts(path, &blk)
      Dir.mktmpdir do |dir|
        Open3.popen2('git', 'clone', path, dir, :chdir => dir) do |i, o, t|
          i.close

          ret = o.read

          errorCode = t.value
          if errorCode != 0
            return false
          end
        end

        yield(dir)
      end
    end
  end
end
