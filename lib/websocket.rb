class Occam < Sinatra::Base
  class WebSocketBackend
    require 'json'
    require 'open3'
    require 'tmpdir'
    require 'fileutils'
    require 'pty'

    KEEPALIVE_TIME = 15 # in seconds

    def initialize(app)
      @app        = app
      @clients    = []
      @terminals  = {}
      @websockets = {}
      @proxys     = {}
    end

    # For 'thin' servers, we need an adapter
    Faye::WebSocket.load_adapter('thin')

    def call(env)
      if Faye::WebSocket.websocket?(env)
        puts "Websocket request"
        req = Rack::Request.new(env)

        # WebSockets logic goes here
        ws = Faye::WebSocket.new(env, ['binary'], {
          ping: KEEPALIVE_TIME
        })

        ws.on :error do |event|
          puts "Websocket Error"
        end

        ws.on :open do |event|
          @terminals[ws] = {}
          @clients << ws
        end

        if Occam::Backend.connected
          # Proxy the websocket to a backend

          # Open Websocket
          backendHost = Occam::Backend.config['host']
          backendPort = Occam::Backend.config['port']

          url = "ws://#{backendHost}:#{backendPort}"
          puts "Opening proxy websocket at #{url}"

          websocket = Faye::WebSocket::Client.new(url, ['binary'], :headers => { "origin" => "localhost" })

          websocket.on :open do |event|
            # When client connects
            puts "Proxy Websocket: Connected"
            @proxys[req] = websocket
          end

          websocket.on :message do |event|
            # Forward received data
            ws.send(event.data)
          end

          websocket.on :error do |event|
            puts "Proxy Websocket: Error"
          end

          websocket.on :close do |event|
            # Server closed connection
            if @proxys[req]
              @proxys[req].close
            end
            @proxys.delete req
            if ws
              ws.close
            end
          end

          ws.on :message do |event|
            # Writes to the backend websocket

            # Talk to open websocket
            websocket = @proxys[req]

            if websocket
              # Respond with data from vnc port
              websocket.send(event.data)
            end
          end
        elsif req.path.start_with?("/vnc/")
          vncPort = req.path[5..-1].to_i

          # Create our own WebSocket to that port locally
          # (or via a backend)

          # Open VNC Websocket
          websocket = Faye::WebSocket::Client.new("ws://localhost:#{vncPort}", ['binary'], :headers => { "origin" => "localhost" })

          websocket.on :open do |event|
            # When client connects
            @websockets[vncPort] = websocket
          end

          websocket.on :message do |event|
            # Forward received data
            begin
              ws.send(event.data)
            rescue
            end
          end

          websocket.on :error do |event|
            puts "SECONDARY WEBSOCKET: video-process-#{vncPort}: Error"
          end

          websocket.on :close do |event|
            # Server closed connection
            puts "Closing VNC connection"
            if @websockets[vncPort]
              @websockets[vncPort].close
            end
            @websockets.delete vncPort
            if ws
              ws.close
            end
          end

          ws.on :message do |event|
            # Writes to the VNC webserver at the requested port

            # Talk to open websocket
            websocket = @websockets[vncPort]

            if websocket
              # Respond with data from vnc port
              websocket.send(event.data)
            end
          end
        else
          ws.on :message do |event|
            begin
              data = JSON.parse(event.data)
            rescue
              data = {}
            end

            puts("WebSocket message")

            tag = data['tag']
            data = data['data']

            request = data["request"]
            terminal = data["terminal"]

            case request
            when "vncOpen"
            when "write"
              # Input writes to the open session
              begin
                @terminals[ws][data["terminal"]][:input].write data["input"]
              rescue
              end
            when "open"
              object_type = data["objectType"]
              object_name = data["objectName"]
              if data.has_key? "spawning"
                case data["spawning"]
                when "build"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam build --to #{object_id} --revision #{object_revision}'"

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  puts "Spawning Thread"
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    PTY.spawn(command) do |output, input, pid|
                      @terminals[ws][data["terminal"]][:input] = input

                      until output.eof? and error.eof?
                        IO.select([output])

                        no_output = false
                        begin
                          output_string = output.read_nonblock(1024)
                        rescue
                          output_string = ""
                        end

                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => output_string,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                    end
                    puts "Done"
                  end
                when "logs"
                  rows = data["rows"]
                  cols = data["cols"]

                  data["data"] = data["data"] || {}
                  job_id = data["data"]["job_id"]

                  # TODO: sanitize (only allow valid uuids and valid revisions) (could base64 the arguments)
                  command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam logs --job #{job_id} --tail'"

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  puts "Spawning Thread"
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    PTY.spawn(command) do |output, input, pid|
                      @terminals[ws][data["terminal"]][:input] = input

                      until output.eof? and error.eof?
                        IO.select([output])

                        no_output = false
                        begin
                          output_string = output.read_nonblock(1024)
                        rescue
                          output_string = ""
                        end

                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => output_string,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                    end
                    puts "Done"
                  end
                when "run"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  workset_arguments = ""
                  if data["data"]["workset_id"]
                    workset_arguments = "--within #{data["data"]["workset_id"]} --within-revision #{data["data"]["workset_revision"]}"
                  end
                  input_arguments = ""
                  if data["data"]["input_id"]
                    input_arguments = "--input #{data["data"]["input_id"]} --input-revision #{data["data"]["input_revision"]}"
                  end
                  command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam run -L json --to #{object_id} --revision #{object_revision} #{workset_arguments} #{input_arguments}'"
                  puts "From data: #{data}"
                  puts "Running native command: #{command}"

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  puts "Spawning Thread"
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    PTY.spawn(command) do |output, input, pid|
                      @terminals[ws][data["terminal"]][:input] = input

                      until output.eof? and error.eof?
                        IO.select([output])

                        no_output = false
                        begin
                          output_string = output.read_nonblock(1024)
                        rescue
                          output_string = ""
                        end

                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => output_string,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                    end
                    puts "Done"
                  end
                when "console"
                  rows = data["rows"]
                  cols = data["cols"]
                  object_id       = data["data"]["object_id"]
                  object_revision = data["data"]["object_revision"]
                  command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam console --to #{object_id} --revision #{object_revision}'"

                  # Create a response thread I guess
                  @terminals[ws][data["terminal"]] = {}
                  puts "Spawning Thread"
                  @terminals[ws][data["terminal"]][:thread] = Thread.new do
                    PTY.spawn(command) do |output, input, pid|
                      @terminals[ws][data["terminal"]][:input] = input

                      until output.eof? and error.eof?
                        IO.select([output])

                        no_output = false
                        begin
                          output_string = output.read_nonblock(1024)
                        rescue
                          output_string = ""
                        end

                        ws.send({
                          :tag => tag,
                          :data => {
                            :response => "data",
                            :output   => output_string,
                            :terminal => data["terminal"],
                          }
                        }.to_json)
                      end
                    end
                    puts "Done"
                  end
                else
                  command = ""
                end
              end
            end
          end
        end

        ws.on :close do |event|
          @terminals[ws].each do |k, v|
            if v.has_key? :thread
              puts "Killing Thread"
              Thread.kill(v[:thread])
            end
          end
          @clients.delete(ws)
          ws = nil
        end

        # Return async Rack response
        ws.rack_response
      else
        @app.call(env)
      end
    end
  end
end
