# OCCAM AMQP
# ----------
# The 'occam-command' queue sits and listens until it hears a command. These
# commands are simply the same commands the cli utility would hear without the
# preceding executable name (occam new workset foo: this would be sent in the
# form of: "new workset foo")
#
# Replies are sent to a per-web-server queue as a AMQP reply-to. (In AMQP, a
# message can contain a reply-to attribute to name where to send a response)
# Currently, each web server instance is generating a random uuid4 for these
# queues.

class Occam
  module AMQP
    require 'bunny'
    require 'thread'

    def self.client
      require 'securerandom'
      # TODO: may have to worry about state / threads here
      # TODO: definitely need to worry about initialization sync
      if @client.nil?
        amqp_config = Occam::Config.configuration['amqp'] || {}
        amqp_config.keys.each do |k|
          amqp_config[(k.intern rescue k) || k] = amqp_config.delete(k)
        end

        @client = :none
        if not amqp_config.empty?
          @client = Bunny.new(amqp_config)
          @client.start

          @reply_bin = SecureRandom.uuid

          @lock      = Mutex.new
          @ret_lock  = Mutex.new
          @condition = ConditionVariable.new
          @ret_condition = ConditionVariable.new
          @response  = ""

          # Wait for reply
          begin
            self.reply_queue.subscribe do |delivery_info, properties, payload|
              @response = payload
              @lock.synchronize do
                @condition.signal
              end
              @ret_lock.synchronize do
                @ret_condition.wait(@ret_lock)
              end
            end
          rescue
            @lock.synchronize do
              @condition.signal
            end
            @ret_lock.synchronize do
              @ret_condition.wait(@ret_lock)
            end
          end
        end
      end

      if @client == :none
        nil
      else
        @client
      end
    end

    def self.channel
      @channel ||= self.client.create_channel
    end

    def self.close
      @client.close
      @client = nil
    end

    def self.queue(name)
      self.channel.queue(name)
    end

    def self.reply_queue()
      self.queue(@reply_bin)
    end

    def self.send_message(message, queue="main")
      queue = self.queue(queue)
      queue.publish(message, :routing_key => queue, :reply_to => @reply_bin)
    end

    def self.send_message_sync(message, queue="main")
      # Clear reply queue
      self.reply_queue.purge

      self.send_message(message, queue)

      @lock.synchronize do
        @condition.wait(@lock)
      end

      ret = @response

      @ret_lock.synchronize do
        @ret_condition.signal
      end

      ret
    end
  end
end
