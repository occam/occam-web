class Occam
  module Worker
    def self.perform(command, type="command")
      dispatch = Occam::Config.configuration['queues'][type]['dispatch']

      if dispatch == "message"
        # Remote Sync task
        client = Occam::AMQP.client
        if client
          Occam::AMQP.send_message_sync(command, "occam-command")
        end
      elsif dispatch == "worker"
        # Remote Async task
        Occam::Job.create(:command => command, :status => :queued, :kind => :run, :codependant => 0, :has_dependencies => 0)
      elsif dispatch == "local"
        puts "Running #{command}"
        # Local Sync task
        # POpen this and yield output
        `occam #{command}`
      end
    end
  end
end
