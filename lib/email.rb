class Occam
  class Email
    def self.send_message(person, message)
      require 'mail'

      mail = Mail.new do
        from 'me@deadreckoning.no-ip.org'
        to   'wilkie05@gmail.com'
        subject 'Blah'
        body 'ok test test'
      end

      mail.delivery_method :sendmail

      mail.deliver
    end

    def self.send_authorization(person)
    end

    def self.send_password_change_token(person)
    end

    def self.send_workset_done(person, workset)
    end
  end
end
