This directory holds other classes that maintain generic application logic or relate or bind to other software/packages.

* `amqp.rb` - Helper class for talking through AMQP. (The messaging protocol to talk to worker nodes)
* `application.rb` - Main application class. Importing this file will import the entire application.
* `backend.rb` - Helper class for talking to other occam web nodes (presumably on worker nodes)
* `config.rb` - Helper class that reads the configuration file and parses out information and fills in defaults and ultimately sets up any software we use (for instance, creates our database connection based on the configuration)
* `email.rb` - Unfinished Helper class that creates email messages for events. This should be a part of occam-worker and not occam-web.
* `git.rb` - Helper class that can issue commands through git. We use this to understand imported objects to display the page about confirmation. Of course, occam-worker could maybe do this for us, though.
* `memcache.rb` - Helper class that can talk to memcache servers.
* `redcarpet.rb` - Augmentations to the markdown parser.
* `websocket.rb` - Helper class for our use of websockets, which allow a sustained connection between web client (somebody's laptop etc) and the web server.
* `worker.rb` - Helper class that will issue OCCAM commands based upon how the node is configured. Can issue commands directly on the web server machine, over AMQP, or by issuing a worker Job (but generally a worker Job isn't the best way to go since most commands issued by the web layer are expected to be fast/synchronous)
