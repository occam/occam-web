class Occam
  # This module gives us limited access to objects found on the filesystem.
  # Jobs store output and configuration information on the filesystem. We
  # want to be able to retrieve this and give it to people through the web
  # interface.
  module Filesystem
    def self.dataExists?(uuid, revision)
      if Occam::Backend.connected
      else
        File.exists?(File.join(self.path('store'), "data"))
      end
    end

    def self.path(uuid, revision, subType = 'objects')
      # Return the path of the object in the local object store
      rel_path = Occam::Config.configuration['paths'][subType]
      if not File.exists?(rel_path)
        Dir.mkdir(rel_path)
      end
      base_path = File.realpath(rel_path)

      code = uuid
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, code_1, code_2, uuid, revision)
    end

    # Retrieves the file data that this object is backing.
    def self.retrieveData(uuid, revision)
      if Occam::Backend.connected
      else
        Filesystem.serveFile(File.join(self.path(uuid, revision, 'store'), "data"))
      end
    end

    # Return an IO stream of the given file so as to upload it to the
    # person requesting it.
    def self.serveFile(path)
      File.open(path, "rb")
    end

    # Return a tar'd file of the contents of a particular path.
    def self.tarPath(path)
    end
  end
end
