class Occam
  require 'zip'

  class Zip
    def initialize(path)
      @path = path
    end

    def exists?(filepath)
      begin
        ::Zip::File.open(@path) do |zipfile|
          return !zipfile.find_entry(filepath).nil?
        end
      rescue
      end

      false
    end

    def retrieveFile(filepath)
      begin
        ::Zip::File.open(@path) do |zipfile|
          return zipfile.read(filepath)
        end
      rescue
      end

      ""
    end

    def retrieveJSON(filepath)
      begin
        return JSON.parse(self.retrieveFile(filepath))
      rescue
      end

      {}
    end

    def retrieveFileInfo(filepath)
      begin
        ::Zip::File.open(@path) do |zipfile|
          return entryToInfo(zipfile.find_entry(filepath)) || {}
        end
      rescue
      end

      {}
    end

    def eachFile(filepath="", filter=nil, &block)
      begin
        ::Zip::File.open(@path) do |zipfile|
          zipfile.each do |entry|
            if entry.name.start_with? filepath
              info = entryToInfo(entry)

              next if filter && filter == :trees && info[:type] != "tree"
              next if filter && filter == :files && info[:type] == "tree"

              yield(info)
            end
          end
        end
      rescue
      end
    end

    private
    def entryToInfo(entry)
      info = {}
      info[:size] = entry.size
      info[:name] = entry.name
      info[:stat] = entry.unix_perms
      info[:type] = "tree"
      if !entry.name_is_directory?
        info[:type] = "file"
      else
        if info[:name].end_with?("/")
          info[:name].chop!
        end
      end
      info[:hash] = entry.crc

      return info
    end
  end
end
