require 'bundler'
Bundler::require

require_relative '../config/environment'

require_relative './websocket'

# Application root
class Occam < Sinatra::Base
  # Use root directory as root
  set :app_file => '.'
  set :server, :thin

  # Use HTML5
  set :haml, :format => :html5

  # Markdown
  Tilt.register Tilt::RedcarpetTemplate, 'md'
  Tilt.prefer Tilt::RedcarpetTemplate
  set :markdown, :layout_engine => :haml,
                 :fenced_code_blocks => true,
                 :smartypants   => true,
                 :layout        => :markdown,
                 :renderer      => Redcarpet::Render::HTML

  # SSL
  set :ssl_certificate, File.join(File.dirname(__FILE__), "..", "key", "server.crt")
  set :ssl_key        , File.join(File.dirname(__FILE__), "..", "key", "server.key")

  # Use sessions
  use Rack::Session::Cookie, :key    => 'rack.session',
                             :path   => '/',
                             :expire_after => 2592000,
                             :secret => Occam::Config.configuration['secret']

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), "..", 'config', 'locales', '*.yml')]

  # SASS
  require 'sass/plugin/rack'
  Sass::Plugin.options[:template_location] = "./views/stylesheets"
  Sass::Plugin.options[:css_location]      = "./public/stylesheets"
  use Sass::Plugin::Rack

  use Rack::MethodOverride
  use Occam::WebSocketBackend

  class RequestLogger
    def initialize(app)
      @app = app
    end

    def call(env)
      request = Rack::Request.new(env)
      if not request.path.end_with?("runs/running")
        puts "-------------------------"
        puts request.path
      end
      status, headers, response = @app.call(env)
      if not request.path.end_with?("runs/running")
        puts "  -> #{status}"
      end
      [status, headers, response]
    end
  end

  use RequestLogger

  # Helpers
  helpers Sinatra::ContentFor

  def self.load_tasks
    Dir[File.join(File.dirname(__FILE__), "tasks", '*.rb')].each do |file|
      require file
    end
  end

  helpers do
    def partial(page, options={})
      if page.to_s.include? "/"
        page = page.to_s.sub /[\/]([^\/]+)$/, "/_\\1"
      else
        page = "_#{page}"
      end
      haml page.to_sym, options.merge!(:layout => false)
    end
  end
end

# Load application source
%w(config helpers controllers models).each do |dir|
  Dir[File.join(File.dirname(__FILE__), "..", dir, '*.rb')].each {|file| require_relative file}
end

require_relative '../lib/redcarpet'
require_relative '../lib/email'
require_relative '../lib/worker'

# The OCCAM server needs an SSL key for replication
require_relative '../lib/https'
Occam::HTTPS.ensureKey

# The OCCAM server should know itself
puts "Looking for path of occam toolchain: "
if system("command -v occam")
  puts "Committing occam-web object"
  `occam commit`
end

# Create default paths
require 'fileutils'

# Create and Use Default OCCAM install path
unless Occam::System.first
  system_paths = {}
  ["objects", "jobs", "worksets"].each do |dir|
    path = File.realdirpath(File.join(occam_path, dir))
    system_paths["#{dir}_path"] = path
  end

  Occam::System.create system_paths
end

#ActiveRecord::Base.logger = nil

# The OCCAM AMQP messaging service
require_relative './amqp'

# The OCCAM backend HTTP service
require_relative './backend'

# Enable chunked streaming (this is required for some git clients)
require_relative './stream'

# Filesystem Retrieval for .occam/store
require_relative './filesystem'

# Zip file support
require_relative './zip'
