class Occam
  class ObjectInput < ActiveRecord::Base
    # Fields

    # subtype
    # object_type

    # Associations

    belongs_to :object,
               :foreign_key => :occam_object_id

    def objects
      if self.object_type == "occam/runnable"
        q = Occam::Object.where(:runnable => 1).where('object_type != ?', 'occam/task')
      else
        q = Occam::Object.where(:object_type => self.object_type)
      end

      if not self.subtype.blank?
        q = q.where(:group => self.subtype)
      end
      q
    end

    def indirect_objects
      sub_query = Occam::ObjectOutput.where(:object_type => self.object_type)
      if not self.subtype.blank?
        sub_query = sub_query.where(:subtype => self.subtype)
      end
      Occam::Object.where(:id => sub_query.select(:occam_object_id))
    end
  end
end
