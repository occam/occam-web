class Occam
  class Configuration < ActiveRecord::Base
    require 'base64'

    # For forms, we encode the keys as urlsafe_base64 so that they can serve
    # as ids. Our validation library optimistically requires them. We will need
    # to decode them before we can store them.
    def self.decode_base64(hash, level = 0)
      hash ||= {}

      if hash.is_a? Hash
        hash.keys.each do |k|
          original_key = k
          v = hash[k]

          if level < 2
            # The first two levels are the ids for the connection and configuration
            # Just skip them

            if v.is_a? Hash
              self.decode_base64(v, level + 1)
            end
          else
            # Decode base 64

            # Add padding back
            if (k.length % 4) > 0
              k = k + ("=" * (4 - (k.length % 4)))
            end

            k = Base64.urlsafe_decode64(k)
            hash[k] = v
            hash.delete original_key

            if v.is_a? Hash
              self.decode_base64(v, level)
            end
          end
        end
      end

      hash
    end

    require 'json'

    # Associations

    belongs_to :object,
               :foreign_key => :occam_object_id

    # Returns the schema in the given format. Defaults to yielding a Hash object
    # describing the schema.
    #
    # format:
    #   :hash => Gives a Hash object. (default)
    #   :json => Gives a string containing a valid JSON representation.
    def schema(format = :hash)
      if self.schema_document_id
        document_id = BSON::ObjectId.from_string(self.schema_document_id)
        document = ConfigurationSchema.first(:id => document_id)
      end

      if document
        case format
        when :hash
          document.serializable_hash
        when :json
          document.to_json
        end
      end
    end
  end
end
