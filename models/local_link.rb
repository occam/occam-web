class Occam
  class LocalLink < ActiveRecord::Base
    # Fields
    # path - The local path that has checked-out the given workset/object

    belongs_to :person
    belongs_to :workset
  end
end
