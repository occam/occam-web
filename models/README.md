These files describe the data models and logic that relate to parts of the system. Typically a model corresponds to a database table.

* `account.rb` - The Account model is the table that contains the hashed passwords used to authenticate a particular account to a Person.
* `authorship.rb` - The Authorship model is a join table that connects a Person to an Object as an "author". Authors generally have full control over objects.
* `bookmark.rb` - The Bookmark model is a record that marks an Object as being bookmarked by a Person. It joins People and Objects.
* `collaboratorship.rb` - The Collaborator model is a join table that connects a Person to an Object as a "collaborator". Collaborators may be given different permissions for objects by authors.
* `experiment.rb` - The Experiment model describes the record for an Experiment object.
* `group.rb` - The Group model describes the record for a Group object.
* `job.rb` - The Job model describes an individual task that has been issued to (or queued upon) an OCCAM worker. Various methods exist that can query or control this job.
* `local_link.rb` - The LocalLink model joins a Person to a Workset with a local path. Generally useful when working in the command line such that changes made can be synchronized automatically whether or not they are made on the command line or in the web view.
* `node.rb` - The Node model describes some OCCAM server that exists somewhere else in the wide wide world.
* `object.rb` - The Object model is a generic record for any object known by our OCCAM node.
* `object_input.rb` - The ObjectInput model joins objects together that are known to be inputs of a particular object. This is used by the Workflow Widget to prune possible objects that will make connections to existing nodes in a workflow.
* `object_output.rb` - The ObjectOutput model joins objects together that are known to be outputs of a particular object. This is used by the Workflow Widget to prune possible objects that will make connections to existing nodes in a workflow.
* `person.rb` - The Person model describes an individual known to our OCCAM node. This record can contain various personal and professional information that a person elects to enter in. This includes an email and an avatar image, etc.
* `provider.rb` - The Provider model describes a capability or environment an object provides. These capabilities are used by the VM builder when it decides what objects to package together to run an object.
* `review_capability.rb` - The ReviewCapability joins a Person and a Workset revision. Whenever such a record exists, the revision indicated by that record will be allowed to be viewed publicly, but with authorship removed. To revoke such access, simply destroy the record.
* `run.rb` - The Run model describes and aggregates work (Jobs) and keeps track of which Experiment, Workset, and Person it belongs to. Also includes methods that will control at a high level the Jobs which are running. For instance, one can cancel a Run.
* `session.rb` - The Session model is a set of records per Person that keep track of open sessions and logins. This is so we can revoke logins remotely.
* `system.rb` - The System model describes the overall administrative record for the system. System settings and configuration and moderation properties exist here. Only persons with an administrative role can view or change them.
* `workset.rb` - The Workset model describes the record for a Workset object.
