class Occam
  class Association < ActiveRecord::Base
    # Fields
    #
    # id            - primary key
    # views_type    - The type of object this is a viewer for
    # views_subtype - The subtype of object this is a view for (if any)

    # Associations
    belongs_to :person
    belongs_to :object,
               :foreign_key => :occam_object_id
  end
end
