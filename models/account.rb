class Occam
  class Account < ActiveRecord::Base
    require 'bcrypt'

    # Attached to a person
    belongs_to :person

    # Fields

    # id              - Unique identifier.

    # active          - Whether or not this person has been moderated

    # private_key     - The RSA private key for this account

    def self.all_inactive
      Account.where(:active => 0)
    end

    def self.all_active
      Account.where(:active => 1)
    end

    # email           - The email for this person.

    # hashed_password - The hash of the password.

    # roles           - A semi-colon (;) delimited string of roles this account has

    # Returns true if the user has the particular role
    def has_role?(role)
      self.roles.to_s.include? ";#{role};"
    end

    # Adds the given role to this user
    def add_role(role)
      roles = self.get_roles
      roles.append(role).uniq!
      self.roles = ";#{roles.join(';')};"
    end

    # Removes the given role if it exists
    def delete_role(role)
      roles = self.get_roles
      roles.delete(role)
      self.roles = ";#{roles.join(';')};"
    end

    def get_roles
      (self.roles or ";;").split(';')[1..-1] or []
    end

    # Create a hash of the password.
    def self.hash_password(password)
      BCrypt::Password.create(password, :cost => Occam::BCRYPT_ROUNDS)
    end

    # Determine if the given password matches the person.
    def authenticated?(password)
      (self.active == 1) && BCrypt::Password.new(hashed_password) == password
    end

    def self.newAccount(opts)
      # Look for existing account with that username, first
      if !Occam::Account.find_by(:username => opts[:username]).nil?
        return nil
      end

      # Spawn task to create the account
      # It will return the new uuid of the created Person

      # Do not allow a blank username or password
      username = opts[:username]
      if username.blank?
        return nil
      end
      password = opts[:password]
      if password.blank?
        return nil
      end

      # Pull out roles requested for this person
      roles = opts[:roles] || []
      if not roles.is_a?(Array)
        roles = [roles]
      end

      # Turn the roles into a list of --role arguments
      roles_args = roles.map{|role| " --role #{Base64.strict_encode64(role.to_s)}"}.join.strip

      # Form the occam command
      command = "new --base64 #{Base64.strict_encode64("person")} #{Base64.strict_encode64(username)} --password #{Base64.strict_encode64(password)} #{roles_args}"

      # Submit the occam command
      # The output will be the new account's Person object uuid
      uuid = Occam::Worker.perform(command).strip

      # Return the Person record after a db lookup
      Occam::Object.findPerson(uuid)
    end
  end
end
