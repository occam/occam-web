class Occam
  class ReviewCapability < ActiveRecord::Base
    # Fields

    # published - DateTime when object was originally published
    def update_timestamps
      now = Time.now.utc
      self[:published] ||= now if !persisted?
    end

    before_save :update_timestamps

    # Associations

    belongs_to :workset
    belongs_to :person
  end
end
