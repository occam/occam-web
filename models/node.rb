class Occam
  class Node < ActiveRecord::Base
    # Fields

    # host          - The name of the host for this node
    # name          - A friendlier name for this node
    # http_port     - The port for http requests
    # http_ssl      - Whether or not http requests are using HTTPS
    # amqp_port     - The port for AMQP requests
    # amqp_username - The username for AMQP access
    # amqp_password - The password for AMQP access
    # capabilities  - Capabilities (such as 'git', 'http', 'amqp', 'worker') delimited by a ';'

    def name
      self[:name] || self.host
    end

    def http_ssl
      (self[:http_ssl] || 0) != 0
    end

    def capabilities
      (self[:capabilities] || ";").strip[1..-1].split(';')
    end
  end
end
