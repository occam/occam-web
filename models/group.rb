class Occam
  class Group < ActiveRecord::Base
    # Fields

    # id        - Unique identifier.

    # Has an object representation
    belongs_to :object,
               :foreign_key => :occam_object_id

    # Returns some aggregate understanding of the status of this group.
    # If all jobs have completed, it will be finished.
    # The rest of the statuses override this in this order:
    # If any job is unqueued, it will be unqueued.
    # If any job is queued, it will be queued.
    # If any job is running, it will be running.
    # If any job has failed, it will have failed.
    def status
      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.experiments.map do |experiment|
        job_status = experiment.status

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    # Removes the given item from the groups's contents list
    def removeItem(itemIndex, workset)
      command = "set --base64 #{Base64.strict_encode64("contains.#{itemIndex}")} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} "

      # Amend workset id/revision
      command << "--within #{Base64.strict_encode64(workset.uid)} "
      command << "--within-revision #{Base64.strict_encode64(workset.revision)} "

      Occam::Worker.perform(command).strip
    end

    def addGroup(workset, name)
      command = "new --base64 #{Base64.strict_encode64("group")} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)
    end

    def addExperiment(workset, name)
      command = "new --base64 #{Base64.strict_encode64("experiment")} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)
    end

    def addObject(workset, name)
      command = "new --base64 #{Base64.strict_encode64("object")} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)
    end

    # Rewrite object helper method to give us a static version of the object.
    alias_method :object_current, :object
    def object
      @object ||= self.object_current
    end

    def to_json(*args)
      self.object.to_json(*args)
    end

    def method_missing(*args, &block)
      if self.object
        self.object.send(*args, &block)
      else
        super(*args, &block)
      end
    end
  end
end
