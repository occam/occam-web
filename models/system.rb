class Occam
  class System < ActiveRecord::Base
    # Fields

    # id                - Unique identifier.
    # host              - hostname of the node.
    # port              - port of the node.

    # moderate_objects  - Whether or not objects must be authorized by
    #                     an admin before they are installed/built.
    # moderate_accounts - Whether or not people must be activated by
    #                     an admin before they can log in.
    # curate_git        - Whether or not git read access is given and
    #                     git repos used by objects are mirrored.
    # curate_hg         - Whether or not mercurial read access is given and
    #                     mercurial repos used by objects are mirrored.
    # curate_svn        - Whether or not svn read access is given and
    #                     svn repos used by objects are mirrored.
    # curate_files      - Whether or not file object read access is given and
    #                     any external files used by objects are mirrored.

    # Will set the host and port according to the request. Returns true when
    # changes were made.
    def self.setHostAndPort(request)
      system = self.retrieve

      if system.host != request.host || system.port != request.port
        system.host = request.host
        system.port = request.port

        system.save

        true
      else
        false
      end
    end

    # Retrieves the canonical system configuration
    def self.retrieve
      Occam::System.all.first
    end

    # Run/retrieve tests
    def self.tests
      # Workset tests:
      if Occam::Backend.connected
        Occam::Backend.downloadJSON('/system/tests')
      else
        begin
          output = Occam::Worker.perform("test --json")
          JSON.parse(output)
        rescue
          {}
        end
      end
    end
  end
end
