class Occam
  # Records the dependency between one Object and another
  class Authorship < ActiveRecord::Base
    # Fields
    #
    # id
    # name (if person_id is null, it could be an empty person)

    belongs_to :person
    belongs_to :workset
  end
end
