class Occam
  class Viewer < ActiveRecord::Base
    # Fields

    # occam_object_id - the Object that this provider points toward
    belongs_to :object,
               :foreign_key => :occam_object_id

    # views_type      - the object type this viewer renders
    # object_subtype  - the subtype of object this views, if any
    # revision        - the tagged revision for the viewer
  end
end
