class Occam
  # These helper methods are for string manipulation or testing.
  # Right now, the only methods here are for internationalization support
  # for right-to-left languages. When we see a right-to-left script, we need
  # to style the page to reflect that.
  module StringHelpers
    def direction(string)
      # Returns :rtl if the string starts with a rtl character
      #         :ltr otherwise
      if string.nil?
        return :ltr
      end

      @rtl_script_types  ||= ["Arabic", "Hebrew"]
      @rtl_script_regexp ||= "[#{@rtl_script_types.map do |script|
        "\\p{#{Regexp.escape(script)}}"
      end.join}]"

      if string.match /^#{@rtl_script_regexp}/
        :rtl
      else
        :ltr
      end
    end
  end

  helpers StringHelpers
end
