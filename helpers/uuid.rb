class Occam
  module UUIDHelper
    def isUUID(uuid)
      uuid.is_a?(String) and uuid.length == 36 and uuid[8]  == '-' and
  uuid[13] == '-' and
  uuid[18] == '-' and
  uuid[23] == '-'
    end
  end

  helpers UUIDHelper
end
