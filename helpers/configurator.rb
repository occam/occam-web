class Occam
  module ConfiguratorHelpers
    def render_ranged_nav(schema, id, sub_id)
      render_nav(schema, "data[#{id}][#{sub_id}]", nil)
    end

    def render_nav(schema, nesting="data", key=nil)
      require 'base64'

      if key
        base64_key = Base64.urlsafe_encode64(key)
        base64_key.chomp!('=')
        base64_key.chomp!('=')
        base64_key.chomp!('=')
      end

      new_nesting = ""
      new_nesting = "[#{base64_key}]" if base64_key

      if schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          ""
        else
          # Output group
          "<ul>" + schema.keys.map { |k|
            v = schema[k]
            if v.is_a? Hash
              inner_base64_key = Base64.urlsafe_encode64(k)
              inner_base64_key.chomp!('=')
              inner_base64_key.chomp!('=')
              inner_base64_key.chomp!('=')

              outer_base64_key = Base64.urlsafe_encode64("#{nesting}#{new_nesting}[#{k}]")
              outer_base64_key.chomp!('=')
              outer_base64_key.chomp!('=')
              outer_base64_key.chomp!('=')

              if v.has_key?("type") && v['type'] == "array"
                group_label = "List"
                if v.has_key?("label") && v["label"].is_a?(String)
                  group_label = v["label"]
                end

                # An array
                "<li data-key='#{inner_base64_key}'><a href='##{outer_base64_key}'>#{group_label}[]</a></li>"
              elsif v.has_key?("type") && !(v["type"].is_a?(Hash))
                # An input value
                ""
              else
                # A group
                group_label = k
                if v.has_key?("label") && v["label"].is_a?(String)
                  group_label = v["label"]
                end
                "<li data-key='#{inner_base64_key}'><a href='##{outer_base64_key}'>#{group_label}</a></li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end

    # This method produces html that lists all input options for a given schema.
    # When values is given, they are used to provide the value of each option.
    # Otherwise, the default values given in the schema are used.
    # When dropdowns is true, an enumerated type will render a dropdown to show
    # all possible options. When false, just the current value or default will
    # be shown.
    def render_config(schema, values=nil, dropdowns=true, key=nil)
      if schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if values && values.has_key?(key)
            value = values[key]
          elsif schema.has_key?("default")
            value = schema["default"]
          end

          type = schema["type"]

          # The default label is the key
          if not schema.has_key?("label")
            schema['label'] = key
          end

          if schema["type"] == "array"
            # Array configuration type
          elsif schema["type"].is_a?(Array) && (dropdowns || values.nil?)
            # Enum type
            "<p class='enum'>#{schema["label"]}</p>" +
            "<div class='select'><select>" + schema["type"].map { |sub_type|
              selected = (value == sub_type)
              "<option#{selected ? " selected='selected'" : ""}>#{sub_type}</option>"
            }.join("") + "</select></div>" +
            "<div class='dots'></div>" +
            "<div class='description'>#{schema["description"]}</div>"
          else
            # Normal key/value pair
            "<p class='#{type}'>#{schema["label"]}</p><p>#{value}</p>" +
            "<div class='dots'></div>" +
            "<div class='description'>#{schema["description"]}</div>"
          end
        else
          # Output group
          sub_values = values
          if values && values.has_key?(key)
            sub_values = values[key]
          end
          "<ul class='configuration-group'>" + schema.keys.map { |k|
            v = schema[k]
            if v.is_a? Hash
              if v.has_key?("type") && !(v["type"].is_a?(Hash))
                # An input value
                "<li>#{render_config(v, sub_values, dropdowns, k)}</li>"
              else
                # A group
                "<li><h2><span class='expand shown'>&#9662;</span>#{k}</h2>#{render_config(v, sub_values, dropdowns, k)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end

    def render_binary_form(object, id)
      return "" if object.nil? or object.binaries.empty?

      base64_key = Base64.urlsafe_encode64("__binaries")
      base64_key.chomp!('=')
      base64_key.chomp!('=')
      base64_key.chomp!('=')

      base64_id = Base64.urlsafe_encode64(id.to_s)
      base64_id.chomp!('=')
      base64_id.chomp!('=')
      base64_id.chomp!('=')

      hash = "data[#{base64_id}][#{base64_key}]"

      "<ul class='configuration-group' data-nesting='binary'>" +
        "<li><label>Binary</label><span class='expand'>[+]</span>" +
          "<div class='select'><select name='#{hash}'>" + object.binaries.map{|binary| "<option>#{binary.name}</option>"}.join("") + "</select></div>" +
          "<div class='dots'></div>" +
          "<div class='description'><p>The binary to execute.</p></div>" +
        "</li>" +
      "</ul>"
    end

    def render_ranged_form(hash, schema, recipes={}, object, id, sub_id)
      render_form(hash, schema, "data[#{id}][#{sub_id}]", nil, false, true, recipes, '', object)
    end

    def render_form(hash, schema, nesting="data", key=nil, blank=false, range=false, recipes={}, header='', object=nil)
      require 'base64'

      if key
        base64_key = Base64.urlsafe_encode64(key)
        base64_key.chomp!('=')
        base64_key.chomp!('=')
        base64_key.chomp!('=')
      end

      new_nesting = ""
      new_nesting = "[#{base64_key}]" if base64_key

      if schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if schema.has_key?("type") && (schema["type"] == "long" || schema["type"] == "port")
            schema["type"] = "int"
          end

          if schema.has_key?("type") && (schema["type"] == "array" || schema["type"] == "hex")
            schema["type"] = "string"
          end

          if schema.has_key?("type") && (schema["type"] == "file")
            schema["type"] = "string"
          end

          # The default label is the key
          if not schema.has_key?("label")
            schema['label'] = key
          end

          if blank
            value = ""
          elsif schema.has_key?("default")
            value = schema["default"]
          end

          if hash && hash.has_key?(key)
            value = hash[key]
          end

          description_div = ""
          if schema.has_key? "description"
            description_div = "<div class='description'>#{render(:markdown, schema["description"])}</div>"
          end

          validations = ""

          if schema["type"] == "int"
            if range
              validations += "data-parsley-type_range='integer' "
            else
              validations += "type='number' "
            end
          elsif schema["type"] == "float"
            if range
              validations += "data-parsley-type_range='number' "
            else
              validations += "data-parsley-type='number' "
            end
          end

          enables = ""
          if schema.has_key? "disables"
            if not schema["disables"].is_a? Array
              schema["disables"] = [schema["disables"]]
            end

            schema["disables"].each_with_index do |entry, i|
              if entry.has_key? "key" or entry.has_key? "sibling"
                disables_key = entry["key"]
                if disables_key
                  selector = "all"
                else
                  disables_key = entry["sibling"]
                  selector = "sibling"
                end
                base64_disables_key = Base64.urlsafe_encode64(disables_key)
                base64_disables_key.chomp!('=')
                base64_disables_key.chomp!('=')
                base64_disables_key.chomp!('=')

                enables += "data-disables-key-#{i}='#{selector}:#{base64_disables_key}' "
              end
              if entry.has_key? "is"
                enables += "data-disables-is-#{i}='#{entry["is"]}' "
              end
            end

            enables += "data-disables-count='#{schema['disables'].length}' "
          end

          if schema.has_key? "enables"
            if not schema["enables"].is_a? Array
              schema["enables"] = [schema["enables"]]
            end

            schema["enables"].each_with_index do |entry, i|
              if entry.has_key? "key" or entry.has_key? "sibling"
                enables_key = entry["key"]
                if enables_key
                  selector = "all"
                else
                  enables_key = entry["sibling"]
                  selector = "sibling"
                end
                base64_enables_key = Base64.urlsafe_encode64(enables_key)
                base64_enables_key.chomp!('=')
                base64_enables_key.chomp!('=')
                base64_enables_key.chomp!('=')

                enables += "data-enables-key-#{i}='#{selector}:#{base64_enables_key}' "
              end
              if entry.has_key? "is"
                enables += "data-enables-is-#{i}='#{entry["is"]}' "
              end
            end

            enables += "data-enables-count='#{schema['enables'].length}' "
          end

          if schema.has_key? "shows"
            if not schema["shows"].is_a? Array
              schema["shows"] = [schema["shows"]]
            end

            schema["shows"].each_with_index do |entry, i|
              if entry.has_key? "key" or entry.has_key? "sibling"
                shows_key = entry["key"]
                if shows_key
                  selector = "all"
                else
                  shows_key = entry["sibling"]
                  selector = "sibling"
                end
                base64_shows_key = Base64.urlsafe_encode64(shows_key)
                base64_shows_key.chomp!('=')
                base64_shows_key.chomp!('=')
                base64_shows_key.chomp!('=')

                enables += "data-shows-key-#{i}='#{selector}:#{base64_shows_key}' "
              end
              if entry.has_key? "is"
                enables += "data-shows-is-#{i}='#{entry["is"]}' "
              end
            end

            enables += "data-shows-count='#{schema['shows'].length}' "
          end

          if schema.has_key? "hides"
            if not schema["hides"].is_a? Array
              schema["hides"] = [schema["hides"]]
            end

            schema["hides"].each_with_index do |entry, i|
              if entry.has_key? "key" or entry.has_key "sibling"
                hides_key = entry["key"]
                if hides_key
                  selector = "all"
                else
                  hides_key = entry["sibling"]
                  selector = "sibling"
                end
                base64_hides_key = Base64.urlsafe_encode64(hides_key)
                base64_hides_key.chomp!('=')
                base64_hides_key.chomp!('=')
                base64_hides_key.chomp!('=')

                enables += "data-hides-key-#{i}='#{selector}:#{base64_hides_key}' "
              end
              if entry.has_key? "is"
                enables += "data-hides-is-#{i}='#{entry["is"]}' "
              end
            end

            enables += "data-hides-count='#{schema['hides'].length}' "
          end

          if schema.has_key? "validations"
            schema["validations"].each do |validation|
              if validation.has_key? "test"
                if range
                  validations += "data-parsley-test_range='#{validation["test"]}' "
                else
                  validations += "data-parsley-test='#{validation["test"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-test_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-test-message='#{validation["message"]}' "
                  end
                end
              end
              if validation.has_key? "min"
                if range
                  validations += "data-parsley-min_range='#{validation["min"]}' "
                else
                  validations += "min='#{validation["min"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-min_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-min-message='#{validation["message"]}' "
                  end
                end
              end
              if validation.has_key? "max"
                if range
                  validations += "data-parsley-max_range='#{validation["max"]}' "
                else
                  validations += "max='#{validation["max"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-max_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-max-message='#{validation["message"]}' "
                  end
                end
              end
            end
            validations += "data-parsley-trigger='focusout' "
          end

          case schema["type"]
          when "datapoints"
            "<label class='datapoints'>#{schema["label"]}</label><span class='expand'>[+]</span><ul class='datapoints'>#{((hash || {})[key] || []).map{|datapoint|
              "<li class='datapoint'>" + "<span class='output' data-object-id='#{datapoint['object']['id']}' data-object-revision='#{datapoint['object']['revision']}'><a href='/objects/#{datapoint['object']['id']}/#{datapoint['object']['revision']}'>output</a></span>" + (datapoint['nesting'] || []).map{|dataPointNesting|
                if dataPointNesting.is_a? String
                  "<span class='key'>#{dataPointNesting}</span>"
                elsif dataPointNesting.is_a? Array
                  "<span class='range'>[#{dataPointNesting.map{ |rangeItem|
                    if rangeItem.is_a? Array
                      if rangeItem.length > 1
                        "<span class='range'><span class='min'>#{rangeItem[0]}</span>..<span class='max'>#{rangeItem[1]}</span></span>"
                      end
                    else
                      "<span class='item'>#{rangeItem}</span>"
                    end
                  }.join}]</span>"
                end
              }.join + "</li>"
            }.join}</ul>" + description_div
          when "int", "float", "string", "color"
            "<label class='#{schema["type"]}'>#{schema["label"]}</label><span class='expand'>[+]</span><input #{validations}#{enables}name='#{nesting}#{new_nesting}' value='#{value}' class='#{schema["type"]}'>" +
            "<div class='dots'></div>" +
            description_div
          when "boolean"
            if blank
              "<label class='boolean'>#{schema["label"]}</label><span class='expand'>[+]</span>" +
              "<div class='select'><select #{validations}#{enables}name='#{nesting}#{new_nesting}'>" + ["true", "false", "___any___"].map { |type|
                if blank
                  selected = ("___any___" == type)
                else
                  selected = (value == type)
                end
                if type == "___any___"
                  "<option#{selected ? " selected='selected'" : ""} value='___any___'>Any</option>"
                else
                  "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
                end
              }.join("") + "</select></div>" +
              "<div class='dots'></div>" +
              description_div
            else
              "<label class='boolean'>#{schema["label"]}</label><span class='expand'>[+]</span><input type='hidden' name='#{nesting}#{new_nesting}' value='off'><input #{validations}#{enables}type='checkbox' name='#{nesting}#{new_nesting}'#{value ? " checked" : ""}>" +
              "<div class='dots'></div>" +
              description_div
            end
          when Array
            if blank
              schema["type"].push("___any___")
            end
            "<label class='enum'>#{schema["label"]}</label><span class='expand'>[+]</span>" +
            "<div class='select'><select #{validations}#{enables}name='#{nesting}#{new_nesting}'>" + schema["type"].map { |type|
              if blank
                selected = ("___any___" == type)
              else
                selected = (value == type)
              end
              if type == "___any___"
                "<option#{selected ? " selected='selected'" : ""} value='___any___'>Any</option>"
              else
                "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
              end
            }.join("") + "</select></div>" +
            "<div class='dots'></div>" +
            description_div
          else
          end
        else
          # Output group
          "<ul class='configuration-group' data-nesting='#{nesting}#{new_nesting}' data-key='#{base64_key}'>" + schema.keys.map { |k|
            v = schema[k]
            if v.is_a? Hash
              new_hash = hash
              if hash && hash.has_key?(key)
                new_hash = hash[key]
              end

              if header.length > 0
                new_header = "#{header}/#{k}"
              else
                new_header = k || ""
              end

              inner_base64_key = Base64.urlsafe_encode64(k)
              inner_base64_key.chomp!('=')
              inner_base64_key.chomp!('=')
              inner_base64_key.chomp!('=')

              outer_base64_key = Base64.urlsafe_encode64("#{nesting}#{new_nesting}[#{k}]")
              outer_base64_key.chomp!('=')
              outer_base64_key.chomp!('=')
              outer_base64_key.chomp!('=')

              if v.has_key?("type") && v['type'] == "array"
                group_label = "List"
                if v.has_key?("label") && v["label"].is_a?(String)
                  group_label = v["label"]
                end

                # An array
                # TODO: handle description
                puts "ARRAY"
                puts new_hash

                base64_key = Base64.urlsafe_encode64(k)
                base64_key.chomp!('=')
                base64_key.chomp!('=')
                base64_key.chomp!('=')

                new_nesting = "[#{base64_key}]"

                "<li class='array' data-key='#{inner_base64_key}'><h2><span class='expand shown'>&#9662;</span><span class='group-label'>#{group_label}</span></h2><ul class='configuration-group'>#{((new_hash || {})[k] || []).each_with_index.map{|item, index|
                  puts nesting
                  puts new_nesting
                  "<li class='element'>" + render_form(item, v['element'] || {}, "#{nesting}#{new_nesting}", index.to_s, blank, range, recipes, new_header) + "</li>"}.join}<input type='button' class='button add-element' value='Add'></input></ul><div class='element' aria-hidden='true'>#{render_form({}, v['element'] || {}, "#{nesting}", k, blank, range, recipes, new_header)}</div></li>"
              elsif v.has_key?("type") && !(v["type"].is_a?(Hash))
                # An input value
                "<li data-key='#{inner_base64_key}'>#{render_form(new_hash, v, "#{nesting}#{new_nesting}", k, blank, range, recipes, new_header)}</li>"
              else
                # A group
                # Rake any recipes for this group
                dropdown = ""
                if recipes.has_key? new_header
                  options = recipes[new_header].map do |recipe|
                    "<option data-template='/objects/#{object.id}/recipes/#{recipe.id}'>#{recipe.name}</option>"
                  end.join('')
                  dropdown = "<div class='recipe'><select>#{options}</select></div>"
                end
                group_label = k
                if v.has_key?("label") && v["label"].is_a?(String)
                  group_label = v["label"]
                end
                "<li data-key='#{inner_base64_key}' id='#{outer_base64_key}'>#{dropdown}<h2><span class='expand shown'>&#9662;</span><span class='group-label'>#{group_label}</span></h2>#{render_form(new_hash, v, "#{nesting}#{new_nesting}", k, blank, range, recipes, new_header)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end
  end

  helpers ConfiguratorHelpers
end
