class Occam
  module TestViewerHelpers
    def render_test_stories(list)
      result = ""

      list.each do |info|
        result << "<div class='story' data-symbol='#{info['result']}'><span class='description'>#{info['message']}</span></div>"
      end

      result
    end

    def render_test_section(hash)
      result = ""

      hash.each do |k, v|
        if v.is_a? Hash
          # Module/Class/Function
          result << "<h2>#{k}</h2><div class='section'>#{render_test_section(v)}</div>"
        elsif v.is_a? Array
          # Function story listing
          result << "<h3>#{k}</h2><div class='stories'>#{render_test_stories(v)}</div>"
        end
      end

      result
    end

    def render_test_viewer(hash)
      "<div class='test-viewer'>" + render_test_section(hash) + "</div>"
    end
  end

  helpers TestViewerHelpers
end
