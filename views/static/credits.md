# Acknowledgements

## System Designer and Lead Developer

* [wilkie](http://wilkie.io)

## Developers

* Chelsea Mafrica
* Junhui Chen
* Jay McAleer
* Gennady Martynenko
* Brian Dicks
* Long Pham
* Ben Moncuso
* Jim Devine
* Nicholas Alberts
* Phillip Faust
* Cullen Strouse
* Christopher Iwaszko

## Artists

OCCAM is supported by the open art from various artists. Without their support
of the commons, we would not be nearly as pretty.

<div style="margin: 15px">
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/gear.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/recipe.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/fork.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/person.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/simulator_large.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/pillar.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/benchmark_large.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/game_large.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/workflowItemConfiguration.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/workflowItemView.png"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/bookmark.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/role_administrator.svg"> </div>
<div style="display: inline-block; width: 30px; text-align: center;"> <img style="vertical-align: middle; max-width: 30px;" src="/images/icons/paper_large.svg"> </div>
</div>

* Gear designed by [Reed Enger](http://www.thenounproject.com/reed) from the [Noun Project](http://thenounproject.com).
* Flask designed by [Renan Ferreira Santos](http://www.thenounproject.com/renanfsdesign) from the [Noun Project](http://thenounproject.com).
* Fork designed by [Dmitry Baranovskiy](http://www.thenounproject.com/DmitryBaranovskiy) from the [Noun Project](http://thenounproject.com).
* User designed by [Andreas Bjurenborg](http://www.thenounproject.com/andreas.bjurenborg) from the [Noun Project](http://thenounproject.com).
* Cpu based on design by [Michael Anthony](http://www.thenounproject.com/m.j.anthony) from the [Noun Project](http://thenounproject.com).
* Column based on design by [Benedikt Martens](http://www.thenounproject.com/Benedikt) from the [Noun Project](http://thenounproject.com).
* Gear/Magnifying Glass based on design by [irene hoffman](http://www.thenounproject.com/i) from the [Noun Project](http://thenounproject.com).
* Game Controller designed by [Jack](http://www.thenounproject.com/SkeletonJack666) from the [Noun Project](http://thenounproject.com).
* Wrench and Screwdriver by [Cheesefork](https://thenounproject.com/Cheesefork) from the [Noun Project](http://thenounproject.com).
* Magnifying Glass based on design by [irene hoffman](http://www.thenounproject.com/i) from the [Noun Project](http://thenounproject.com).
* Bookmark based on design by [icon 54](http://www.thenounproject.com/icon54app) from the [Noun Project](http://thenounproject.com).
* Gear by [Shawn Erdely](https://thenounproject.com/shawn4) from the [Noun Project](http://thenounproject.com).
* Diagram by [Aha-Soft](https://thenounproject.com/ahasoft) from the [Noun Project](http://thenounproject.com).

## Open Source Software

The OCCAM software project is proud to use and support the following software
projects. This is nowhere near a complete list of open source software that we
make use of. That list is almost impractical to generate!

### Server

* [Ruby](https://www.ruby-lang.org/en/) - an expressive scripting language.
* [haml](http://haml.info/) - markup preprocessor for html.
* [minitest](https://github.com/seattlerb/minitest) - minimal testing framework.
* [mocha](http://gofreerange.com/mocha/docs/) - mocking/stubbing framework for testing.
* [postgres](http://www.postgresql.org/) - robust SQL database implementation for deployed environments.
* [Sinatra](http://www.sinatrarb.com/) - a minimalistic web framework for Ruby.
* [sqlite](http://www.sqlite.org/) - public domain SQL implementation for development.
* [thin](http://code.macournoyer.com/thin/) - a minimal webserver we use for development.
* [chronic_duration](https://github.com/hpoydar/chronic_duration) - a library to pretty print time durations.
* [i18n](http://rails-i18n.org/wiki) - internationalization library.
* [redcarpet](https://github.com/vmg/redcarpet) - markdown parsing library.
* [capybara](https://jnicklas.github.io/capybara/) - acceptance testing selenium driver for the front-end.

### Worker

* [Python 3](https://python.org) - A robust scripting language.
* [rabbitpy](https://pypi.python.org/pypi/rabbitpy) - AMQP client library.
* [pycrypto](https://pypi.python.org/pypi/pycrypto) - Crypto helper library.
* [alembic](http://docs.alembic.io/python/) - Data migration generation tool.
* [SQLAlchemy](http://www.sqlalchemy.org/) - SQL Database tool and library.
* [pyyaml](http://pyyaml.org/) - YAML parser and library.
* [bcrypt](https://pypi.python.org/pypi/bcrypt) - Python bcrypt implementation.
* [pytest](http://pytest.org/) - Python testing framework.

### Javascript and Visualization Tools

* [Spectrum](https://bgrins.github.io/spectrum/) - Javascript color picker library.
* [node-uuid.js](https://github.com/broofa/node-uuid) - Javascript UUID generation library.
* [noVNC](http://kanaka.github.io/noVNC/) - Javascript vnc client library.
* [jQuery](http://jquery.com/) - Javascript document manipulation library.
* [d3](http://d3js.org/) - Data-oriented Visualization and SVG library.
