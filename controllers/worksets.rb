class Occam
  # Create a new workset
  post '/worksets' do
    status 404 and return if current_person.nil?

    workset_id = current_person.addWorkset(params["name"])
    # TODO: errors

    redirect "/worksets/#{workset_id}"
  end

  # Retrieve a list of tags for autocomplete
  get '/worksets/tags' do
    content_type 'application/json'
    Occam::Workset.all_tags_json(params["term"])
  end

  # Workset tabs
  get '/worksets/:uuid/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:uuid/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:uuid/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:uuid/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:uuid/review_links/?' do
    params[:tab] = "review_links"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/review_links/?' do
    params[:tab] = "review_links"
    objectShow(params)
  end
  get '/worksets/:uuid/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end

  get '/worksets/:uuid/history/?' do
    objectHistory(params)
  end
  get '/worksets/:uuid/:revision/history/?' do
    objectHistory(params)
  end
  get '/worksets/:uuid/history.json' do
    objectHistory(params, 'application/json')
  end
  get '/worksets/:uuid/:revision/history.json' do
    objectHistory(params, 'application/json')
  end

  # Show object
  get '/worksets/:uuid/?' do
    objectShow(params)
  end
  get '/worksets/:uuid/:revision/?' do
    objectShow(params)
  end

  # Deletes a ReviewLink
  delete '/worksets/:uuid/:revision/review-links/:id' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      review = ReviewCapability.find_by(:id => params[:id])
      if review.nil?
        status 404
      else
        review.destroy

        redirect "/worksets/#{workset.uid}/#{workset.revision}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision/fork' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo

      render :haml, :"worksets/fork", :locals => {
        :errors        => nil,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => workset.authors.first,
        :revision      => revision,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/fork' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      workset = workset.fork(params["name"], current_person)

      #redirect "/people/#{current_person.uid}"
      redirect "/worksets/#{workset.uid}"
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/groups' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if workset.revision == params["revision"]
        redirectBase = true
      else
        redirectBase = false
      end

      if params["revision"]
        workset.revision = params["revision"]
      end

      revisions = workset.addGroup(params["name"])
      revisions = revisions.split("\n")

      workset_revision = revisions[0]
      group_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{uuid}/groups/#{group_id}"
      else
        redirect "/worksets/#{uuid}/#{workset_revision}/groups/#{group_id}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/objects' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if workset.revision == params["revision"]
        redirectBase = true
      else
        redirectBase = false
      end

      if params["revision"]
        workset.revision = params["revision"]
      end

      type = params["type"]

      case type
      when "experiment"
        revisions = workset.addExperiment(params["name"])
        revisions = revisions.split("\n")
        path = "experiments"
      when "group"
        revisions = workset.addGroup(params["name"])
        revisions = revisions.split("\n")
        path = "groups"
      else
        revisions = workset.addObject(params["name"], params["type"])
        revisions = revisions.split("\n")
        path = "objects"
      end

      workset_revision = revisions[0]
      object_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{uuid}/#{path}/#{object_id}"
      else
        redirect "/worksets/#{uuid}/#{workset_revision}/#{path}/#{object_id}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/experiments' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if workset.revision == params["revision"]
        redirectBase = true
      else
        redirectBase = false
      end

      if params["revision"]
        workset.revision = params["revision"]
      end

      revisions = workset.addExperiment(params["name"])
      revisions = revisions.split("\n")

      workset_revision = revisions[0]
      experiment_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{uuid}/experiments/#{experiment_id}"
      else
        redirect "/worksets/#{uuid}/#{workset_revision}/experiments/#{experiment_id}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  # Add author to an existing workset
  post '/worksets/:uuid/:revision/authors' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset
      if params["object-id"]
        person = Occam::Object.findPerson(params["object-id"])
      else
        person = Person.find_by(:username => params["username"])
      end

      if person
        if workset.can_edit?(current_person)
          workset.authors << person
          workset.save
        end
      else
        if workset.can_edit?(current_person)
          workset.authorships << Occam::Authorship.new(:name => params["username"])
          workset.save
        end
      end

      if request.referrer
        redirect request.referrer
      else
        redirect objectURL(workset, "/", {
          :object => workset,
          :revision => workset.revision
        })
      end
    else
      status 404
    end
  end

  # Delete an Authorship
  delete '/worksets/:uuid/:revision/authors/:id' do
    workset    = Occam::Object.findWorkset(params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]
    authorship = Occam::Authorship.find_by(:id => params[:id])

    if workset && workset.can_edit?(current_person) && authorship && authorship.workset_id == workset.id
      if authorship.person_id.nil? || workset.authors.length > 1
        authorship.destroy
      end

      if request.referrer
        redirect request.referrer
      else
        redirect objectURL(workset, "/", {
          :object => workset,
          :revision => workset.revision
        })
      end
    else
      status 404
    end
  end

  # Delete an Collaboratorship
  delete '/worksets/:uuid/:revision/collaborators/:id' do
    workset          = Occam::Object.findWorkset(params[:uuid])
    revision         = params[:revision]
    uuid             = params[:uuid]
    collaboratorship = Occam::Collaboratorship.find_by(:id => params[:id])

    if workset && workset.can_edit?(current_person) && collaboratorship && collaboratorship.workset_id == workset.id
      collaboratorship.destroy

      if request.referrer
        redirect request.referrer
      else
        redirect objectURL(workset, "/", {
          :object => workset,
          :revision => workset.revision
        })
      end
    else
      status 404
    end
  end

  # Add collaborator to an existing workset
  post '/worksets/:uuid/:revision/collaborators' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset
      if params["person"]
        person = Occam::Object.findPerson(params["person"])
      else
        person = Person.find_by(:username => params["username"])
      end

      if person
        if workset.can_edit?(current_person)
          workset.collaborators << person
          workset.save
        end
      else
        if workset.can_edit?(current_person)
          workset.collaboratorships << Occam::Collaboratorship.new(:name => params["username"])
          workset.save
        end
      end

      if request.referrer
        redirect request.referrer
      else
        redirect objectURL(workset, "/", {
          :object => workset,
          :revision => workset.revision
        })
      end
    else
      status 404
    end
  end

  # Edit an existing workset
  post '/worksets/:uuid' do
    workset = Occam::Object.findWorkset(params[:uuid])

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if params.has_key? "private"
        workset.object.private = params["private"].to_i
        workset.object.save
      end
      if params.has_key? "review"
        workset.revision = params["review"]
        workset.allow_review(current_person)
      end
      fields = params.select { |k,_| ["description", "name", "tags"].include? k }
      unless fields.empty?
        revisions = workset.update_info(fields)
        workset.revision = revisions[-1]
      end
      redirect "/worksets/#{workset.uid}/#{workset.revision}"
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision/edit' do
    workset  = Occam::Object.findWorkset(params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo

      render :haml, :"worksets/new", :locals => {
        :errors        => nil,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => workset.authors.first,
        :revision      => revision,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end
end
