class Occam
  # Show object
  get '/worksets/:workset_uuid/groups/:uuid/?' do
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/?' do
    objectShow(params)
  end

  # Groups tabs
  get '/worksets/:workset_uuid/groups/:uuid/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end

  # History View
  get '/worksets/:workset_uuid/groups/:uuid/history/?' do
    objectHistory(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/history/?' do
    objectHistory(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/history.json' do
    objectHistory(params, 'application/json')
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/history.json' do
    objectHistory(params, 'application/json')
  end

  post '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/groups' do
    group    = Occam::Object.findGroup(params[:uuid])
    uuid     = params[:uuid]
    revision = params[:revision]

    workset = Occam::Object.findWorkset(params[:workset_uuid])
    if workset.nil?
      status 404
      return
    end

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if group.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      workset.revision = params[:workset_revision]

      object_info = workset.objectInfo

      if revision
        group.revision = params["revision"]
      end

      revisions = group.addGroup(workset, params["name"])
      revisions = revisions.split("\n")

      workset_revision = revisions[0]
      group_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{workset.uid}/groups/#{group_id}"
      else
        redirect "/worksets/#{workset.uid}/#{workset_revision}/groups/#{group_id}"
      end
    else
      # Unauthorized
      status 406
    end
  end

  post '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/objects' do
    group    = Occam::Object.findGroup(params[:uuid])
    uuid     = params[:uuid]
    revision = params[:revision]

    workset = Occam::Object.findWorkset(params[:workset_uuid])
    if workset.nil?
      status 404
      return
    end

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if group.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if revision
        group.revision = params["revision"]
      end

      object_info = group.objectInfo

      workset.revision = params[:workset_revision]

      type = params["type"]

      case type
      when "experiment"
        revisions = group.addExperiment(workset, params["name"])
        revisions = revisions.split("\n")
        path = "experiments"
      when "group"
        revisions = group.addGroup(workset, params["name"])
        revisions = revisions.split("\n")
        path = "groups"
      else
        revisions = group.addObject(workset, params["name"])
        revisions = revisions.split("\n")
        path = "objects"
      end

      workset_revision = revisions[0]
      experiment_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{workset.uid}/#{path}/#{experiment_id}"
      else
        redirect "/worksets/#{workset.uid}/#{workset_revision}/#{path}/#{experiment_id}"
      end
    else
      # Unauthorized
      status 406
    end
  end
end
