class Occam
  get '/' do
    System.setHostAndPort(request)

    haml :"index"
  end

  get '/styleguide' do
    haml :"static/styleguide"
  end

  get '/about' do
    markdown :"static/about", :layout => :static
  end

  get '/acknowledgements' do
    markdown :"static/credits", :layout => :static
  end

  # 404 route
  not_found do
    markdown :"static/404", :layout => :static
  end

  # 500 route
  error do
    markdown :"static/500", :layout => :static
  end

  # Successfully get the 404 page
  get '/404' do
    markdown :"static/404", :layout => :static
  end

  # Successfully get the 500 page
  get '/500' do
    markdown :"static/500", :layout => :static
  end

  # HTTPS Public Key
  get '/public_key/?' do
    content_type "text/plain"
    send_file "key/server.crt"
  end
end
