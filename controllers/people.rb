class Occam
  # Get a list of all people
  get '/people' do
    people = Occam::Person.all
    render :haml, :"people/index", :locals => {
      :people => people
    }
  end

  # Create a new person
  post '/people' do
    person = Occam::Account.newAccount :username => params["username"],
                                       :password => params["password"],
                                       :roles    => :experimentalist

    if person.nil?
      # Error. Username is probably taken in this case.
      errors = Person.new.errors
      errors.add(:username, "is taken.")
      return render(:haml, :"people/new", :locals => {
        :errors => errors
      })
    end

    # Sign in
    new_session = person.activate_session
    session[:person_id] = person.id
    session[:nonce] = new_session.nonce

    redirect "/people/#{person.uid}"
  end

  # Form to create a new person
  get '/people/new' do
    render :haml, :"people/new", :locals => {:errors => nil}
  end

  # Update person/profile information or activate
  post '/people/:uuid' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil?
      status 404
    else
      status 406 and return if current_person.nil?

      columns = {}
      if current_person.id == person.id
        # Normal people can change their username, etc
        columns = params.select { |k,v| [
            "username",
            "email",
            "bio",
            "name",
            "organization"
          ].include?(k) }
      else
        status 406
      end

      unless columns.empty?
        email = columns.delete "email"

        obj = person.object
        obj.update_columns columns
        obj.save

        if email
          person.email = email
          person.save
        end
      end

      if current_person == person
        redirect "/people/#{person.uid}"
      end
    end
  end

  # Retrieve a specific person page
  get '/people/:uuid' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil?
      status 404
    else
      render :haml, :"people/show", :locals => {
        :errors         => nil,
        :tab            => nil,
        :help           => params['help'],
        :person         => person,
        :worksets       => person.worksets,
        :collaborations => person.collaborations
      }
    end
  end

  # Retrieve a person's bookmark list
  get '/people/:uuid/bookmarks' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil?
      status 404
    else
      render :haml, :"people/show", :locals => {
        :errors         => nil,
        :tab            => "bookmarks",
        :help           => params['help'],
        :person         => person,
        :worksets       => person.worksets,
        :collaborations => person.collaborations
      }
    end
  end

  # Retrieves the avatar image for the particular person
  get '/people/:uuid/avatar' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil?
      status 404
    else
      redirect person.avatar_url((params["size"] || 64).to_i)
    end
  end

  # Retrieves the avatar image for the particular person
  get '/people/:uuid/:revision/avatar' do
    person = Occam::Object.findPerson(params[:uuid], params[:revision])

    if person.nil?
      status 404
    else
      redirect person.avatar_url((params["size"] || 64).to_i)
    end
  end

  # Form to edit an person's profile
  get '/people/:uuid/edit' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil?
      status 404
    else
      if current_person.id != person.id
        status 406
      else
        render :haml, :"people/edit", :locals => {
          :errors  => nil,
          :person => current_person
        }
      end
    end
  end

  # Posts a new bookmark
  post '/people/:uuid/bookmarks' do
    if current_person && current_person.uid == params[:uuid]
      object = Occam::Object.findObject(params["object_id"])
      if object.nil?
        status 404
        return
      end

      if object.respond_to? :object
        object = object.object
      end

      bookmark = Occam::Bookmark.create(:occam_object_id => object.id,
                                        :person_id       => current_person.id)

      if request.referrer
        redirect request.referrer
      else
        redirect "/people/#{current_person.uid}/bookmarks"
      end
    else
      status 404
    end
  end

  # Removes a bookmark
  delete '/people/:uuid/bookmarks/:id' do
    if current_person && current_person.uid == params[:uuid]
      bookmark = Occam::Bookmark.where(:id => params[:id]).first

      if bookmark.nil?
        status 404
        return
      end

      bookmark.destroy

      if request.referrer
        redirect request.referrer
      else
        redirect "/people/#{current_person.uid}/bookmarks"
      end
    else
      status 404
    end
  end

  # Posts a new Association
  post '/people/:uuid/associations' do
    if current_person && current_person.uid == params[:uuid]
      object = Occam::Object.findObject(params["object_id"])
      if object.nil?
        status 404
        return
      end

      if object.respond_to? :object
        object = object.object
      end

      views_type    = params["views_type"]
      views_subtype = params["views_subtype"]

      association = Occam::Association.where(:views_type => views_type, :views_subtype => views_subtype).first

      if association.nil?
        association = Occam::Association.create(:occam_object_id => object.id,
                                                :person_id       => current_person.id,
                                                :views_type      => views_type,
                                                :views_subtype   => views_subtype)
      else
        association.occam_object_id = object.id
        association.save
      end

      if request.referrer
        redirect request.referrer
      else
        redirect "/people/#{current_person.uid}/associations"
      end
    else
      status 404
    end
  end

  # Removes an Association
  delete '/people/:uuid/associations/:id' do
    if current_person && current_person.uid == params[:uuid]
      association = Occam::Association.where(:id => params[:id]).first

      if association.nil?
        status 404
        return
      end

      association.destroy

      if request.referrer
        redirect request.referrer
      else
        redirect "/people/#{current_person.uid}/associations"
      end
    else
      status 404
    end
  end

  # Show all Runs for this Person
  get '/people/:uuid/runs' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"people/runs", :locals => {
          :errors => nil,
          :person => person
        }
      when 'application/json'
        # Return run metadata
        content_type 'application/json'
        person.runs.map(&:to_hash).to_json
      end
    end
  end

  # Show all Runs for this Person explicitly as json
  get '/people/:uuid/runs/finished.json' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      # Return run metadata
      content_type 'application/json'
      person.runs.map(&:to_hash).to_json
    end
  end

  # Show all finished Runs for this Person
  get '/people/:uuid/runs/finished' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"people/runs", :locals => {
          :errors => nil,
          :filter => "finished",
          :person => person
        }
      when 'application/json'
        # Return run metadata
        content_type 'application/json'
        person.done_runs.map(&:to_hash).to_json
      end
    end
  end

  # Show all finished Runs for this Person explicitly as json
  get '/people/:uuid/runs/finished.json' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      # Return run metadata
      content_type 'application/json'
      person.done_runs.map(&:to_hash).to_json
    end
  end

  # Show all running Runs for this Person
  get '/people/:uuid/runs/running' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"people/runs", :locals => {
          :errors => nil,
          :filter => "running",
          :person => person
        }
      when 'application/json'
        # Return run metadata
        content_type 'application/json'
        person.running_runs.map(&:to_hash).to_json
      end
    end
  end

  # Add an object to the recently used list for the given person as long as
  # that person is logged in
  post '/people/:uuid/recentlyUsed' do
    # Retrieve command JSON
    begin
      input = JSON.load(request.body)
    rescue
      # Parsing failure
      status 400
      return
    end

    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      object = Occam::Object.findObject(input["object_id"], input["object_revision"])

      # Add the object to the recently used objects list if not in there
      # already.
      if object
        # TODO: privacy controls
        # TODO: when it already exists, we can update its timestamp
        # TODO: we then want to remove LRU fashion the oldest record
        if not person.recentlyUsedObjects.include?(object)
          person.recentlyUsedObjects << object
        end
      else
        # Could not find the object requested
        status 400

        # TODO: error message
      end
    end
  end

  # Show all running Runs for this Person explicitly as json
  get '/people/:uuid/runs/running.json' do
    person = Occam::Object.findPerson(params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      # Return run metadata
      content_type 'application/json'
      person.running_runs.map(&:to_hash).to_json
    end
  end
end
