class Occam
  # Activate an account
  post '/accounts/:id' do
    if !current_account.has_role?(:administrator)
      status 404
      return
    end

    account = Occam::Account.where(:id => params[:id]).first

    if account.nil?
      status 404
      return
    end

    if params["active"] == "1"
      account.active = 1
    elsif params["active"] == "0"
      account.active = 0
    end

    account.save

    redirect '/admin'
  end

  # Delete an account
  delete '/accounts/:id' do
    if !current_account.has_role?(:administrator)
      status 404
      return
    end

    account = Occam::Account.where(:id => params[:id]).first

    if account
      account.destroy
    else
      status 404
      return
    end

    redirect '/admin'
  end
end
