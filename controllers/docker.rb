class Occam
  DOCKER_PORT = 5000
  def dockerStartRegistry
    # TODO: backend and port from configuration
    `docker run -d -p #{DOCKER_PORT}:#{DOCKER_PORT} --name registry registry:2`
    `docker start registry`
  end

  def dockerPushObject(uuid, revision)
    puts "ok: pushing #{uuid} #{revision}"
    `docker push localhost:5000/#{uuid}:#{revision}`
  end

  def dockerEndRegistry
    `docker stop registry`
  end

  def dockerPassthrough
    require 'thread'

    path = request.path

    new_request = Net::HTTP::Get.new(path)
    new_request['Accept'] = request['Accept']

    http = Net::HTTP.new('localhost', DOCKER_PORT)

    ret = nil

    lock = Mutex.new
    resource = ConditionVariable.new
    resource_wire = false

    stream_lock_A = Mutex.new
    stream_lock_B = Mutex.new
    stream_wire_A = false
    stream_wire_B = false
    streaming_A = ConditionVariable.new
    streaming_B = ConditionVariable.new

    rsp = nil
    chunk = nil

    request_thread = Thread.new do
      http.request(new_request) do |docker_response|
        rsp = docker_response

        status docker_response.code.to_i

        docker_response.header.each_header do |key, value|
          headers[key] = value
        end

        # UNLOCK STREAM
        lock.synchronize do
          resource_wire = true
          resource.signal
        end

        # WAIT FOR THE STREAM TO COMPLETE
        if docker_response.code.to_i != 404
          docker_response.read_body do |inner_chunk|
            stream_lock_A.synchronize do
              if not stream_wire_A
                streaming_A.wait(stream_lock_A)
              end
              stream_wire_A = false
            end
            chunk = inner_chunk
            stream_lock_B.synchronize do
              stream_wire_B = true
              streaming_B.signal
            end
          end

          stream_lock_A.synchronize do
            if not stream_wire_A
              streaming_A.wait(stream_lock_A)
            end
            stream_wire_A = false
          end
          chunk = ""
          stream_lock_B.synchronize do
            stream_wire_B = true
            streaming_B.signal
          end
        end
      end
    end

    lock.synchronize do
      if not resource_wire
        resource.wait(lock)
      end
      resource_wire = false
    end

    docker_response = rsp

    stream do |out|
      while true do
        # TELL HTTP THREAD TO PULL A CHUNK
        stream_lock_A.synchronize do
          stream_wire_A = true
          streaming_A.signal
        end
        stream_lock_B.synchronize do
          if not stream_wire_B
            streaming_B.wait(stream_lock_B)
          end
          stream_wire_B = false
        end

        if chunk.length == 0
          break
        end

        out << chunk
      end
    end
  end

  # V2

  # Docker Version Status
  get '/v2/?' do
    dockerStartRegistry
    dockerPassthrough
  end

  # Catalog
  get '/v2/_catalog' do
    dockerStartRegistry
    dockerPassthrough
  end

  # Image Tag List
  get '/v2/:uuid/tags/list' do
    dockerStartRegistry
    dockerPassthrough
  end

  # Manifest
  get '/v2/:uuid/manifests/latest' do
    dockerStartRegistry
    dockerPassthrough
  end

  get '/v2/:uuid/manifests/:revision' do
    dockerStartRegistry
    dockerPassthrough
  end

  # Layers
  get '/v2/*/blobs/:tarsum' do |image, tarsum|
    dockerStartRegistry
    dockerPassthrough
  end

  # Layer Existence
  head '/v2/*/blobs/:digest' do |image, digest|
    status 404
  end

  get '/v2/_ping' do
    dockerStartRegistry
    dockerPassthrough
  end

  # v1
  get '/v1/_ping' do
    dockerStartRegistry
    dockerPassthrough
  end

  get '/v1/repositories/*' do |image|
    dockerStartRegistry
    dockerPassthrough
  end

  get '/resources/docker-container/:uuid/:revision' do
    # Determine image name
    # TODO: sanitize
    imageName = "occam/#{params[:uuid]}:#{params[:revision]}"

    # Open "docker save" with the image name
    command = "docker save #{imageName}"

    # Return a byte stream
    content_type "application/octet-stream"

    # Stream stdout to the network
    stream do |out|
      IO.popen(command, File::RDWR) do |pipe|
        while !pipe.eof?
          block = pipe.read(8192)
          out << block
        end
      end
    end
  end
end
