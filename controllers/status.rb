class Occam
  get '/status' do
  end

  get '/status/caja' do
    haml :"status/caja"
  end

  get '/status/caja-document-write' do
    haml :"status/caja-document-write", :layout => false
  end

  get '/status/caja-function-call' do
    haml :"status/caja-function-call", :layout => false
  end
end
