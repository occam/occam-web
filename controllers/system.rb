class Occam
  # Get the system configuration
  get '/system' do
    system = Occam::System.all.first
    nodes = Occam::Node.all
    render :haml, :"system/show", :locals => {
      :system => system,
      :nodes  => nodes,
    }
  end

  # Update the system configuration
  post '/system' do
    if !current_account.has_role?('administrator')
      status 404
      return
    end

    system = Occam::System.all.first

    if params["moderate_accounts"] == 'on'
      system.moderate_accounts = 1
    else
      system.moderate_accounts = 0
    end

    if params["moderate_objects"] == 'on'
      system.moderate_objects = 1
    else
      system.moderate_objects = 0
    end

    system.save!

    redirect '/admin'
  end
end
