require 'fileutils'

# Migrate database
system('rake db:migrate')

FileUtils.mkdir_p 'occam'

# Load system
require_relative './lib/application'

# Fake configuration
base_path = File.join(File.realpath(File.dirname(__FILE__)), 'occam')
sims_path = File.join(base_path, 'simulators')
trcs_path = File.join(base_path, 'traces')
jobs_path = File.join(base_path, 'jobs')

FileUtils.mkdir_p sims_path
FileUtils.mkdir_p trcs_path
FileUtils.mkdir_p jobs_path

System.create(:simulators_path => sims_path,
              :traces_path     => trcs_path,
              :jobs_path       => jobs_path)

# Apply ordering to value hash
def tag_order(hash)
  hash.keys.each do |key|
    if hash[key].is_a? Hash
      hash[key] = tag_order(hash[key])
    end
  end

  hash[:__ordering] = hash.keys
  hash
end

INPUT = <<INPUT
{
  "num_chans": {
    "type": "int",
    "default": 1,
    "label": "Number of Channels",
    "validations": [
      {
        "min":    "1"
      },
      {
        "test":    "log2(x) == floor(log2(x))",
        "message": "Must be a power of two"
      }
    ],
    "description": "The number of *logically independent* channels. That is, each with a separate memory controller. Should be a power of 2."
  },
  "jedec_data_bus_bits": {
    "type": "int",
    "default": 64,
    "label": "JEDEC Data Bus Bitsize",
    "validations": [
      {
        "min":    "64"
      },
      {
        "test":    "x % 64 == 0",
        "message": "Must be a factor of 64"
      }
    ],
    "description": "Always 64 for DDRx. If you want multiple *ganged* channels, set this to N*64"
  },
  "trans_queue_depth": {
    "type": "int",
    "default": 32,
    "label": "Transaction Queue Depth",
    "validations": [
      {
        "min":    "1"
      }
    ],
    "description": "Transaction queue. That is, CPU-level commands such as 'READ 0xbeef'"
  },
  "cmd_queue_depth": {
    "type": "int",
    "default": 32,
    "label": "Command Queue Depth",
    "validations": [
      {
        "min":    "1"
      }
    ],
    "description": "Command queue. That is, DRAM-level commands such as 'CAS 544, RAS 4'"
  },
  "epoch_length": {
    "type": "int",
    "label": "Epoch Length",
    "default": 100000,
    "validations": [
      {
        "min":    "1"
      }
    ],
    "description": "The length of an epoch in cycles. That is, the granularity of the simulation."
  },
  "row_buffer_policy": {
    "type": ["close_page", "open_page"],
    "default": "open_page",
    "label": "Row Buffer Policy",
    "description": ""
  },
  "address_mapping_scheme": {
    "type": ["scheme1", "scheme2", "scheme3", "scheme4", "scheme5", "scheme6", "scheme7"],
    "default": "scheme2",
    "label": "Address Mapping Scheme",
    "description": "For multiple independent channels, use scheme7 since it has the most parallelism."
  },
  "scheduling_policy": {
    "type": ["rank_then_bank_round_robin", "rank_then_bank_round_robin"],
    "default": "rank_then_bank_round_robin",
    "label": "Scheduling Policy",
    "description": ""
  },
  "queuing_structure": {
    "type": ["per_rank", "per_rank_per_bank"],
    "default": "per_rank",
    "label": "Queuing Structure",
    "description": ""
  },
  "total_row_accesses": {
    "type": "int",
    "default": 4,
    "label": "Total Row Accesses",
    "validations": [
      {
        "over":    "0",
        "message": "must be greater than 0"
      }
    ],
    "description": "The maximum number of open page requests to send to the same row before forcing a row close. (to prevent starvation)"
  },

  "Debugging Parameters": {
    "debug_trans_q": {
      "type": "boolean",
      "default": "false",
      "label": "Output Transaction Queue Debugging Statements"
    },
    "debug_cmd_q": {
      "type": "boolean",
      "default": "false",
      "label": "Output Command Queue Debugging Statements"
    },
    "debug_addr_map": {
      "type": "boolean",
      "default": "false",
      "label": "Output Address Map Debugging Statements"
    },
    "debug_bus": {
      "type": "boolean",
      "default": "false",
      "label": "Output Bus Debugging Statements"
    },
    "debug_bankstate": {
      "type": "boolean",
      "default": "false",
      "label": "Output Bankstate Debugging Statements"
    },
    "debug_banks": {
      "type": "boolean",
      "default": "false",
      "label": "Output Bank Debugging Statements"
    },
    "debug_power": {
      "type": "boolean",
      "default": "false",
      "label": "Output Power Debugging Statements"
    }
  }
}
INPUT

OUTPUT = <<OUTPUT
{
  "data": {
    "channels": [
      {
        "id": {
          "type": "int",
          "label": true
        },
        "returnTransactionsSize": {
          "type": "int",
          "units": "bytes"
        },
        "returnTransactions": {
          "type": "int",
          "units": "transactions"
        },
        "pendingTransactionsSize": {
          "type": "int",
          "units": "bytes"
        },
        "pendingTransactions": {
          "type": "int",
          "units": "transactions"
        },
        "bandwidth": {
          "type": "float",
          "units": "foo"
        },
        "ranks": [
          {
            "averageActPrePower": {
              "type": "float",
              "units": "watts"
            },
            "averageBackgroundPower": {
              "type": "float",
              "units": "watts"
            },
            "averageBurstPower": {
              "type": "float",
              "units": "watts"
            },
            "averageRefreshPower": {
              "type": "float",
              "units": "watts"
            },
            "averagePower": {
              "type": "float",
              "units": "watts"
            },
            "writes": "int",
            "writesSize": {
              "type": "int",
              "units": "bytes"
            },
            "reads": "int",
            "readsSize": {
              "type": "int",
              "units": "bytes"
            },
            "latency": [
              {
                "max": "int",
                "amount": "int",
                "min": "int"
              }
            ],
            "banks": [
              {
                "latency": {
                  "type": "float",
                  "units": "nanoseconds"
                },
                "bandwidth": {
                  "type": "float",
                  "units": "GB/s"
                },
                "id": {
                  "type": "int",
                  "label": true
                }
              }
            ]
          }
        ]
      }
    ]
  }
}
OUTPUT

SIM_INPUT = <<SIM_INPUT
{"num_chans":1,"jedec_data_bus_bits":64,"trans_queue_depth":32,"cmd_queue_depth":32,"epoch_length":100000,"row_buffer_policy":"open_page","address_mapping_scheme":"scheme2","scheduling_policy":"rank_then_bank_round_robin","queuing_structure":"per_rank","total_row_accesses":4}
SIM_INPUT

SIM_OUTPUT = <<SIM_OUTPUT
{"data": {"channels": [{"returnTransactionsSize": 33408, "pendingTransactions": 2, "bandwidth": 2.074, "ranks": [{"readsSize": 31232, "reads": 488, "banks": [{"id": "0", "latency": "135.779", "bandwidth": "0.207"}, {"id": "1", "latency": "105.042", "bandwidth": "0.143"}, {"id": "2", "latency": "159.700", "bandwidth": "0.302"}, {"id": "3", "latency": "124.688", "bandwidth": "0.191"}, {"id": "4", "latency": "150.065", "bandwidth": "0.389"}, {"id": "5", "latency": "170.562", "bandwidth": "0.318"}, {"id": "6", "latency": "141.454", "bandwidth": "0.318"}, {"id": "7", "latency": "137.856", "bandwidth": "0.207"}], "writes": 34, "averageRefreshPower": 0.055, "averageBurstPower": 0.711, "averageActPrePower": 0.18, "averagePower": 2.762, "averageBackgroundPower": 1.816, "id": 0, "writesSize": 2176, "latency": [{"max": 29, "amount": 58, "min": 20}, {"max": 39, "amount": 47, "min": 30}, {"max": 49, "amount": 35, "min": 40}, {"max": 59, "amount": 36, "min": 50}, {"max": 69, "amount": 30, "min": 60}, {"max": 79, "amount": 16, "min": 70}, {"max": 89, "amount": 35, "min": 80}, {"max": 99, "amount": 18, "min": 90}, {"max": 109, "amount": 19, "min": 100}, {"max": 119, "amount": 26, "min": 110}, {"max": 129, "amount": 30, "min": 120}, {"max": 139, "amount": 25, "min": 130}, {"max": 149, "amount": 15, "min": 140}, {"max": 159, "amount": 17, "min": 150}, {"max": 169, "amount": 22, "min": 160}, {"max": 179, "amount": 12, "min": 170}, {"max": 189, "amount": 7, "min": 180}, {"max": 199, "amount": 12, "min": 190}, {"max": 209, "amount": 13, "min": 200}, {"max": 219, "amount": 1, "min": 210}, {"max": 229, "amount": 4, "min": 220}, {"max": 239, "amount": 6, "min": 230}, {"max": 249, "amount": 1, "min": 240}, {"max": 279, "amount": 3, "min": 270}]}], "returnTransactions": 522, "pendingTransactionsSize": 10000, "id": 0}]}, "warnings": [{"message": "Cannot create memory system with 2048MB, defaulting to minimum size of 4096MB"}], "errors": []}
SIM_OUTPUT

RECIPE_1 = <<RECIPE_1
{"num_chans":"1","jedec_data_bus_bits":"64","trans_queue_depth":"32","cmd_queue_depth":"32","epoch_length":"100000","row_buffer_policy":"open_page","address_mapping_scheme":"scheme2","scheduling_policy":"rank_then_bank_round_robin","queuing_structure":"per_rank","total_row_accesses":"4"}
RECIPE_1

RECIPE_2 = <<RECIPE_2
{"num_chans":"2","jedec_data_bus_bits":"64","trans_queue_depth":"32","cmd_queue_depth":"32","epoch_length":"100000","row_buffer_policy":"open_page","address_mapping_scheme":"scheme2","scheduling_policy":"rank_then_bank_round_robin","queuing_structure":"per_rank","total_row_accesses":"4"}
RECIPE_2

input  = SimulatorInputSchema.create(tag_order(JSON.parse(INPUT)))
output = SimulatorOutputSchema.create(tag_order(JSON.parse(OUTPUT)))

# Fake simulators
Simulator.create(:name          => "DRAMSim2",
                 :tags            => ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards", "memory architecture", "DRAM architecture"],
                 :description   => "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM modules which comprise system storage, and the bus by which they communicate. All major components in a modern memory system are modeled as their own respective objects within the source, including: ranks, banks, command queue, the memory controller, etc.\n\nThe overarching goal is to have a simulator that is extremely small, portable, and accurate. The simulator core has a well-defined interface which allows it to be CPU simulator agnostic and should be easily modifiably to work with any simulator.  This core has no external run time or build time dependencies and has been tested with g++ on Linux as well as g++ on Cygwin on Windows.",
                 :authors       => ";Rosenfeld, P.;Cooper-Balis, E.;Jacob, B.;",
                 :website       => "https://wiki.umd.edu/DRAMSim2",
                 :organization  => "University of Maryland",
                 :license       => "BSD",
                 :output_schema_document_id => output.id.to_s,
                 :input_schema_document_id  => input.id.to_s)

recipe_1 = Configuration.create(JSON.parse(RECIPE_1))

Recipe.create(:simulator => Simulator.first,
              :name => "Single Channel",
              :configuration_document_id => recipe_1.id.to_s)

recipe_2 = Configuration.create(JSON.parse(RECIPE_2))

Recipe.create(:simulator => Simulator.first,
              :name => "Dual Channel",
              :configuration_document_id => recipe_2.id.to_s)

Job.create(:simulator    => Simulator.first,
           :kind         => "install",
           :status       => "finished",
           :elapsed_time => 5.0)

Job.create(:simulator    => Simulator.first,
           :kind         => "build",
           :status       => "finished",
           :elapsed_time => 145.0)

sim_input  = Configuration.create(JSON.parse(SIM_INPUT))
sim_output = Result.create(JSON.parse(SIM_OUTPUT))

# Fake experiments
e =
Experiment.create(:name                      => "Test",
                  :tags                      => ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards", "memory architecture", "DRAM architecture"],
                  :simulator                 => Simulator.first,
                  :configuration_document_id => sim_input.id.to_s,
                  :results_document_id       => sim_output.id.to_s)

Job.create(:experiment   => Experiment.first,
           :kind         => "run",
           :status       => "finished",
           :elapsed_time => 234.45)
