# OCCAM Web Interface

This project serves as the front-end website for the OCCAM system.

## Project Layout

```
:::text

object.json            - the OCCAM object metadata for this project
Gemfile                - the listing of ruby library dependencies (bundler)
Gemfile.lock           - (previous generated) locked versions of those dependencies
Rakefile               - ruby task file invoked with 'rake'
config.ru              - rackup configuration invokved with 'rackup'
dev_start.sh           - starts the server with SSL, intended for developers

lib                    - application code
|- application.rb      - main app and helpers
|- amqp.rb             - AMQP (messaging protocol) support code
|- backend.rb          - for communication with other web nodes in your cluster
|- config.rb           - loads the system's OCCAM configuration
|- email.rb            - SMTP (email protocol) support code
|- git.rb              - git (version control) support code
|- zip.rb              - zip support code
|- https.rb            - SSL (end-to-end encryption protocol) support code to generate certs if none are available
|- redcarpet.rb        - special markdown code
|- stream.rb           - HTTP streaming implementation when we need to force routes to do chunked transfers (git + gnutls bug workarounds)
|- websocket.rb        - WebSocket (sustained client-server browser TCP connections) support and logic
\- worker.rb           - a set of functions for performing OCCAM actions through whatever means is available

helpers                - assorted functions to aid views
|- accept.rb           - routines that aid in selecting which format to render information
|- cache.rb            - routines that aid in selecting what HTTP cache metadata to return
|- configurator.rb     - routines to render configuration forms
|- results.rb          - routines to render structured data and results.
|- schema_helper.rb    - routines to render configuration schemas.
|- roles.rb            - routines to handle role-based authentication.
|- session.rb          - routines to handle session-based authentication.
|- string.rb           - routines to handle right-to-left and language detection.
|- test_viewer.rb      - routines to render test harness output.
|- uuid.rb             - routines to check whether or not a given string is a UUID.
\- documentation.rb    - routines to render docs (DEPRECATED)

config
|- environment.rb       - configuration of different environments.
\- application.rb       - configuration of the site.

models                  - data models and representation
|- person.rb            - user people and authorization
|- experiment.rb        - experiment information
|- workset.rb           - an object that serves as a root collection for project organization
|- group.rb             - an object specifically for grouping other objects
|- object.rb            - the main record for objects in OCCAM
|- job.rb               - contains the jobs for the workers
|- account.rb           - contains information about authorized users
|- authorship.rb        - a join table for People marked as authors of Object
|- collaboratorship.rb  - a join table for People marked as collaborators of Object
|- bookmark.rb          - joins People to Objects for bookmarking objects
|- provider.rb          - a record that marks an Object as being a provider for some capability
|- node.rb              - a record describing other OCCAM sites known to this one
|- run.rb               - a record describing a task that has been created
|- session.rb           - a record describing authorizations to the site
|- system.rb            - a single record for metadata and permissions for the site/system
\- review_capability.rb - a marker for allowing a reviewer access to an object (with authorship redacted)

controllers             - url routing
|- objects.rb           - object retrieval, the bulk of the logic on the site
|- experiments.rb       - routes for experiments
|- groups.rb            - group specific access
|- worksets.rb          - workset specific access
|- runs.rb              - task specific routes
|- people.rb            - routes for people
|- sessions.rb          - logging on/off routes
|- static.rb            - contains routes for simple text content
|- search.rb            - relates to searching functionality
|- system.rb            - administration routes
|- task.rb              - remote task creation
|- providers.rb         - lists objects that provide certain capabilities or environments for other objects
|- workflows.rb         - some routes for managing workflows (add/remove nodes)
|- store.rb             - file resources and retrieval
|- git.rb               - git protocol
\- hg.rb                - mercurial protocol

views                   - html markup and templates
|- stylesheets          - CSS (via SASS/SCSS) for the site
|- objects              - object pages
|- experiments          - experiment specific pages
|- worksets             - workset specific pages
|- groups               - group specific pages
|- jobs                 - job specific pages
|- workflows            - templates for generating workflow diagrams
|- people               - pages for people
|- sessions             - pages for logging on
|- system               - pages for describing the site and system and global metadata
|- admin                - pages for administration and management of the site
|- \_errors.haml         - template for displaying form errors
|- \_footer.haml         - template for the bottom navigation bar
|- \_head.haml           - template for the HEAD section
|- \_scripts.haml        - template for listing the javascript to append to the BODY
|- \_spinner.haml        - template for the spinner
|- \_topbar.haml         - template for the top navigation bar
|- index.haml           - the landing page
|- layout.haml          - the main template for all dynamic pages
|- markdown.haml        - the main template for static markdown content
\- static.haml          - the main template for other static content

public                  - external assets (always available via the webserver)
|- css                  - static stylesheets
|- images               - static images
|- fonts                - static fonts
|- videos               - contains the video on the landing page
\- js                   - javascript files

spec                    - tests
|- acceptance           - tests general site behavior
|- javascripts          - tests client-side js code
|- models               - tests database interaction and other data logic
|- controllers          - tests for routes and renders
\- helper.rb            - script containing common test harness setup/teardown code

key                     - the collection of generated certificates (if one isn't provided)
|- server.crt           - (generated) the server certificate
\- server.key           - (generated) the server's public key

occam-install           - a script that will install OCCAM from this node
|- install.tmp          - the template for generating the script
|- install.sh           - (generated) the script that installs a full OCCAM instance via the site the script is downloaded from
\- object.json          - the OCCAM object metadata for OCCAM
```

## Usage

Install ruby, git, and build tools (gcc 4.8+) separately using your system
package manager. Then install ruby's library manager:

```
:::text
gem install bundler
```

Clone the occam repo:

```
:::text
git clone https://bitbucket.org/occam/occam-web
```

Install postgres and have it running. Ensure the database is loaded via the occam tool:

```
:::text
occam initialize
```

To run a production server (on port 3000):

```
:::text
thin start -R config.ru
```

You can specify a port (such as port 80) using `-p` but it is recommended
to use a more powerful web server and multiple instances of our application
layer.

To start multiple servers, in this case 2, add `-s`:

```
:::text
thin start -R config.ru -s 2
```

This will run them in the background on ports 3000 and 3001. Set up your
web server to route to those ports. We recommend lighttpd.

## Development

Install git and ruby separately.
With ruby installed, install bundler if you haven't already:

```
:::text
gem install bundler
```

Clone this repo:

```
:::text
git clone https://bitbucket.org/occam/occam-web
```

Inside the project directory, install dependencies:

```
:::text
bundle install
```

For just sqlite and no postgres:

```
:::text
bundle install --without production
```

Fake some data, if you would like:

```
:::text
ruby fake_data.rb
```

Run a development server (by default, this project uses 'thin'):

```
:::text
rackup
```

To run a dev server with SSL, which you will need for testing server-to-server communication:

```
:::text
./dev_start.sh
```

This will spawn a local webserver listening on port 9292 at localhost:

```
:::text
http://localhost:9292/
```

or, if using SSL:

```
:::text
https://localhost:9292/
```

## Testing

To run the back-end tests, first ensure that you have built a test database:

```
:::text
occam initialize --test
```

And then inside the project directory, invoke:

```
:::text
rake test
```

For convenience, several shortcuts are available to speed up testing turn around. To run a particular model/controller test, or even a particular file (in this case, model/controller Foo):

```
:::text
rake test:model[foo]
rake test:controller[foo]
rake test:file[foo.rb]
```

## Open Source

This project makes use of several open source technologies:

* [haml](http://haml.info/) - markup preprocessor for html.
* [minitest](https://github.com/seattlerb/minitest) - minimal testing framework.
* [mocha](http://gofreerange.com/mocha/docs/) - mocking/stubbing framework for testing.
* [postgres](http://www.postgresql.org/) - robust SQL database implementation for production.
* [Ruby](https://www.ruby-lang.org/en/) - an expressive scripting language.
* [Sinatra](http://www.sinatrarb.com/) - a minimalistic web framework for Ruby.
* [sqlite](http://www.sqlite.org/) - public domain SQL implementation for development.
* [thin](http://code.macournoyer.com/thin/) - a minimal webserver we use for development.
* [chronic_duration](https://github.com/hpoydar/chronic_duration) - a library to pretty print time durations.
