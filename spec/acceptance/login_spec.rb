require_relative 'helper'

describe "login", :type => :feature do
  before :each do
    @person = Occam::Person.create(:username => "wilkie", :password => "foobar")
  end

  it "should sign me in when I specify a valid username/password" do
    visit '/login'

    fill_in 'username', :with => "wilkie"
    fill_in 'password', :with => "foobar"

    click_button "login"
    current_path.must_equal "/people/#{@person.id}"
  end

  it "should not log me in when I specify the wrong username and password" do
    visit '/login'

    fill_in 'username', :with => "cullen"
    fill_in 'password', :with => "barfoo"

    click_button "login"
    current_path.must_equal "/login"

    page.has_css?('.errors ul li').must_equal true
  end

  it "should not log me in when I specify the wrong password on a valid username" do
    visit '/login'

    fill_in 'username', :with => "wilkie"
    fill_in 'password', :with => "barfoo"

    click_button "login"
    current_path.must_equal "/login"

    page.has_css?('.errors ul li').must_equal true
  end
end
