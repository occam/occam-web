require_relative '../helper'
require 'capybara'
require 'capybara/dsl'
require_relative '../../lib/application'

# Do not dispatch commands
class Occam
  module Worker
    def self.perform(command, type="command")
      ""
    end
  end
end

class MiniTest::Unit::TestCase
  include Capybara::DSL

  alias_method :teardown_old, :teardown
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver

    teardown_old
  end
end

Capybara.app = Occam
Capybara.register_driver :rack_test do |app|
    Capybara::RackTest::Driver.new(app, :headers =>  { 'User-Agent' => 'Capybara' })
end

def login(username)
  @person = Occam::Person.create(:username => username, :password => "foobar")

  visit '/login'

  fill_in 'username', :with => username
  fill_in 'password', :with => "foobar"

  click_button "login"

  @person
end
