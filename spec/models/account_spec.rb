require_relative "helper"
require_model "account"
require_model "person"

def create_account(options)
  account = Occam::Account.create(:person => nil,
                                  :username => options[:username],
                                  :active => options[:active])

  account
end

describe Occam::Account do
  describe "::newAccount" do
    it "should not allow creation with an existing username" do
      account = create_account(:username => "wilkie",
                               :password => "foo")

      person = Occam::Account.newAccount(:username => "wilkie")

      person.nil?.must_equal true
    end
  end

  describe "::all_inactive" do
    it "should list only inactive people" do
      account = create_account(:username => "active_a",
                               :active   => 1,
                               :password => "foo")
      account = create_account(:username => "active_b",
                               :active   => 1,
                               :password => "foo")
      account = create_account(:username => "inactive_a",
                               :active   => 0,
                               :password => "foo")

      Occam::Account.all_inactive.count.must_equal 1
      Occam::Account.all_inactive.first.username.must_equal "inactive_a"
    end
  end

  describe "::all_active" do
    it "should list only active people" do
      account = create_account(:username => "active_a",
                               :active   => 1,
                               :password => "foo")
      account = create_account(:username => "active_b",
                               :active   => 1,
                               :password => "foo")
      account = create_account(:username => "inactive_a",
                               :active   => 0,
                               :password => "foo")

      Occam::Account.all_active.count.must_equal 2
      Occam::Account.all_active.first.username.must_equal "active_a"
    end
  end
end
