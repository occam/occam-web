require_relative "../helper"

# Force authentication to use less crypto rounds
Occam.send(:remove_const, :"BCRYPT_ROUNDS")
Occam.const_set(:"BCRYPT_ROUNDS", 1)

def require_model(name)
  require_relative "../../models/#{name}"
end

# Do not dispatch commands
class Occam
  module Worker
    def self.perform(command, type="command")
      ""
    end
  end
end
