require_relative "helper"
require_model "person"
require_model "object"
require_model "bookmark"
require_model "session"

describe Occam::Person do
  describe "#avatar_url" do
    it "should use gravatar when email is set" do
      a = Occam::Person.create(:username => "jane",
                               :email    => "jane@janemary.com")
      a.avatar_url(48).must_match /^https:\/\/www.gravatar.com\/avatar\//
    end

    it "should pass the size to gravatar when email is set" do
      a = Occam::Person.create(:username => "jane",
                               :email    => "jane@janemary.com")
      a.avatar_url(57).must_match /\?size=57$/
    end

    it "should use a default image when email is not set" do
      a = Occam::Person.create(:username => "jane")
      a.avatar_url(48).must_equal "/images/person.png"
    end

    it "should use the default large image when size is large" do
      a = Occam::Person.create(:username => "jane")
      a.avatar_url(96).must_equal "/images/person_large.png"
    end
  end

  describe "#ssh_public_key" do
    it "should retrieve the SSH public key string from openssl" do
      SSHKey = mock()
      key_return = mock()
      key_return.stubs(:ssh_public_key).returns("SSH_KEY")
      SSHKey.expects(:new).with("PUBLIC_KEY").returns(key_return)

      a = Occam::Person.create(:username   => "jane",
                               :public_key => "PUBLIC_KEY")

      a.ssh_public_key.must_equal "SSH_KEY"
    end
  end

  describe "#ssh2_public_key" do
    it "should retrieve the SSH2 public key string from openssl" do
      SSHKey = mock()
      key_return = mock()
      key_return.stubs(:ssh2_public_key).returns("SSH2_KEY")
      SSHKey.expects(:new).with("PUBLIC_KEY").returns(key_return)

      a = Occam::Person.create(:username   => "jane",
                               :public_key => "PUBLIC_KEY")

      a.ssh2_public_key.must_equal "SSH2_KEY"
    end
  end

  describe "#rsa_public_key" do
    it "should retrieve the RSA public key string from openssl" do
      OpenSSL = Class.new
      OpenSSL::PKey = Class.new
      OpenSSL::PKey::RSA = mock()
      OpenSSL::PKey::RSA.expects(:new).with("PUBLIC_KEY").returns("RSA_KEY")

      a = Occam::Person.create(:username   => "jane",
                               :public_key => "PUBLIC_KEY")

      a.rsa_public_key.must_equal "RSA_KEY"
    end
  end

  describe "#bookmarked?" do
    before :each do
      @person = Occam::Person.create(:username   => "jane")
      @object_in  = Occam::Object.new(nil, :name => "foo",
                                      :uid  => "blah",
                                      :objectInfo => {})
      @object_in.save
      @object_out = Occam::Object.new(nil, :name => "foo",
                                      :uid  => "blar",
                                      :objectInfo => {})
      @object_out.save
      @bookmark = Occam::Bookmark.create(:occam_object_id => @object_in.id)
      @person.bookmarks << @bookmark
    end

    it "should return false when the given object isn't bookmarked" do
      @person.bookmarked?(@object_out).must_equal false
    end

    it "should return true when the given object is bookmarked" do
      @person.bookmarked?(@object_in).must_equal true
    end
  end

  describe "#bookmarkFor" do
    before :each do
      @person = Occam::Person.create(:username   => "jane")
      @object_in  = Occam::Object.new(nil, :name => "foo",
                                      :uid  => "blah",
                                      :objectInfo => {})
      @object_in.save
      @object_out = Occam::Object.new(nil, :name => "foo",
                                      :uid  => "blar",
                                      :objectInfo => {})
      @object_out.save

      @bookmark = Occam::Bookmark.create(:occam_object_id => @object_in.id)
      @person.bookmarks << @bookmark
    end

    it "should return nil when the given object isn't bookmarked" do
      @person.bookmarkFor(@object_out).nil?.must_equal true
    end

    it "should return the bookmark record when the given object is bookmarked" do
      @person.bookmarkFor(@object_in).id.must_equal @bookmark.id
    end
  end

  describe "#name" do
    it "should return the username if no other name is known" do
      object = Occam::Object.new(nil, :name => "", :objectInfo => {})
      object.save
      person = Occam::Person.create(:username => "jane",
                                    :occam_object_id => object.id)
      person.name.must_equal("jane")
    end

    it "should return the name associated with the object metadata for the person if it exists" do
      object = Occam::Object.new(nil, :name => "Jane Smith", :objectInfo => {})
      object.save
      person = Occam::Person.create(:username => "jane",
                                    :occam_object_id => object.id)
      person.name.must_equal("Jane Smith")
    end
  end

  describe "#addWorkset" do
    it "should invoke an occam worker with the given name" do
      Occam::Worker.expects(:perform).returns("")

      object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "blah", :objectInfo => {})
      object.save
      person = Occam::Person.create(:username => "jane",
                                    :occam_object_id => object.id)
      person.addWorkset("foo")

      # TODO: ensure the command is correct
    end
  end

  describe "#activate_session" do
    before :each do
      object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "blah", :objectInfo => {})
      object.save
      @person = Occam::Person.create(:username => "jane",
                                     :occam_object_id => object.id)

      @sessions = []
      @person.stubs(:sessions).returns(@sessions)
    end

    it "should use a secure nonce when creating a session record" do
      SecureRandom.expects(:hex).with(32).returns("NONCE")

      @person.activate_session
    end

    it "should store the secure nonce with the session record" do
      SecureRandom.stubs(:hex).with(32).returns("NONCE")
      Occam::Session.expects(:create).with(has_entry(:nonce => "NONCE")).returns("SESSION")

      @person.activate_session
    end

    it "should store the session as associated with the Person" do
      SecureRandom.stubs(:hex).with(32).returns("NONCE")
      Occam::Session.stubs(:create).with(has_entry(:nonce => "NONCE")).returns("SESSION")

      @person.activate_session
      @sessions.length.must_equal 1
    end
  end

  describe "#active_session_for?" do
    before :each do
      object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "blah", :objectInfo => {})
      object.save
      @person = Occam::Person.create(:username => "jane",
                                     :occam_object_id => object.id)

      SecureRandom.stubs(:hex).with(32).returns("NONCE")
      @active_session = @person.activate_session
    end

    it "should return true when the given nonce has a Session record" do
      @person.active_session_for?("NONCE").must_equal true
    end

    it "should return false when the given nonce does not have a Session record" do
      @person.active_session_for?("VOID").must_equal false
    end
  end

  describe "#deactivate_session_for" do
    it "should remove the Person's Session record with the given nonce" do
      object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "blah", :objectInfo => {})
      object.save
      @person = Occam::Person.create(:username => "jane",
                                     :occam_object_id => object.id)

      SecureRandom.stubs(:hex).with(32).returns("NONCE")
      @active_session = @person.activate_session

      @person.deactivate_session_for("NONCE")
      @person.sessions.count.must_equal 0
    end

    it "should not remove another person's Session if they share the same nonce" do
      object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "blah", :objectInfo => {})
      object.save
      @person = Occam::Person.create(:username => "jane",
                                     :occam_object_id => object.id)

      object = Occam::Object.new(nil, :name => "Janet Smock", :uid => "blah", :objectInfo => {})
      object.save
      @personB = Occam::Person.create(:username => "janet",
                                      :occam_object_id => object.id)

      SecureRandom.stubs(:hex).with(32).returns("NONCE")
      @active_session  = @person.activate_session
      @active_sessionB = @personB.activate_session

      @person.deactivate_session_for("NONCE")
      @personB.sessions.count.must_equal 1
    end
  end
end
