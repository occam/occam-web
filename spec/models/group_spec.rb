require_relative "helper"
require_model "object"
require_model "local_link"
require_model "person"
require_model "authorship"
require_model "review_capability"
require_model "collaboratorship"
require_model "workset"
require_model "group"

describe Occam::Group do
  before :each do
    @object = Occam::Object.new(nil, :uid => "GROUP_UUID", :name => "group", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    @object.save
    @group = Occam::Group.create(:occam_object_id => @object.id)

    object = Occam::Object.new(nil, :uid => "WORKSET_UUID", :name => "workset", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    object.save
    @workset = Occam::Workset.create(:occam_object_id => @object.id)
  end

  describe "#addGroup" do
    it "should invoke an occam worker with the given name and workset" do
      Occam::Worker.expects(:perform).returns("")
      @group.addGroup(@workset, "foo")
    end
  end

  describe "#addExperiment" do
    it "should invoke an occam worker with the given name and workset" do
      Occam::Worker.expects(:perform).returns("")
      @group.addExperiment(@workset, "foo")
    end
  end

  describe "#addObject" do
    it "should invoke an occam worker with the given name and workset" do
      Occam::Worker.expects(:perform).returns("")
      @group.addObject(@workset, "foo")
    end
  end
end
