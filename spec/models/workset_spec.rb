require_relative "helper"
require_model "object"
require_model "local_link"
require_model "person"
require_model "authorship"
require_model "review_capability"
require_model "collaboratorship"
require_model "workset"

describe Occam::Workset do
  before :each do
    @object = Occam::Object.new(nil, :uid => "WORKSET_UUID", :name => "workset", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    @object.save
    @workset = Occam::Workset.create(:occam_object_id => @object.id)

    object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "person1", :objectInfo => {})
    object.save
    @person = Occam::Person.create(:username => "jane",
                                   :occam_object_id => object.id)

    object = Occam::Object.new(nil, :name => "Jim Smash", :uid => "person2", :objectInfo => {})
    object.save
    @author = Occam::Person.create(:username => "jim",
                                   :occam_object_id => object.id)

    object = Occam::Object.new(nil, :name => "Janet Smock", :uid => "person3", :objectInfo => {})
    object.save
    @collaborator = Occam::Person.create(:username => "janet",
                                         :occam_object_id => object.id)

    @workset.collaborators << @collaborator
    @workset.authors       << @author
  end

  describe "#is_author?" do
    it "should return false when person given is nil" do
      @workset.is_author?(nil).must_equal false
    end

    it "should return false when person given is not a collaborator nor author" do
      @workset.is_author?(@person).must_equal false
    end

    it "should return false when person given is a collaborator" do
      @workset.is_author?(@collaborator).must_equal false
    end

    it "should return true when person given is an author" do
      @workset.is_author?(@author).must_equal true
    end
  end

  describe "#is_collaborator?" do
    it "should return false when person given is nil" do
      @workset.is_collaborator?(nil).must_equal false
    end

    it "should return false when person given is not a collaborator nor author" do
      @workset.is_collaborator?(@person).must_equal false
    end

    it "should return true when person given is a collaborator" do
      @workset.is_collaborator?(@collaborator).must_equal true
    end

    it "should return true when person given is an author" do
      @workset.is_collaborator?(@author).must_equal true
    end
  end

  describe "#can_review?" do
    before :each do
      object = Occam::Object.new(nil, :name => "workset", :objectInfo => {}, :private => 1)
      object.save
      @private_workset = Occam::Workset.create(:occam_object_id => object.id)

      @private_workset.collaborators << @collaborator
      @private_workset.authors       << @author

      object = Occam::Object.new(nil, :name => "workset", :objectInfo => {}, :private => 1, :revision => "CURRENT_REVISION")
      object.save
      @review_workset = Occam::Workset.create(:occam_object_id => object.id)

      @review_workset.collaborators << @collaborator
      @review_workset.authors       << @author

      @review_workset.review_capabilities.create(:person => @author,
                                                 :revision => "CURRENT_REVISION")

      @review_revision = "REVIEW_REVISION"
      @review_workset.review_capabilities.create(:person => @author,
                                                 :revision => @review_revision)
    end

    it "should return true when the workset is public even when there are no review capabilities" do
      @workset.can_review?.must_equal true
    end

    it "should return false when the workset is private and there are no review capabilities" do
      @private_workset.can_review?.must_equal false
    end

    it "should return true when the workset is private and there is a review capability for the current revision" do
      @review_workset.can_review?.must_equal true
    end

    it "should return true when the workset is private and there is a review capability for the given revision" do
      @review_workset.can_review?(@review_revision).must_equal true
    end

    it "should return false when the workset is private and there is not a review capability for the given revision" do
      @review_workset.can_review?("BOGUS_REVISION").must_equal false
    end
  end

  describe "#allow_review" do
    it "should create a review capability for the current revision when none exist" do
      @workset.allow_review(@author)

      @workset.review_capabilities.length.must_equal 1
    end

    it "should create a review capability with the current revision when none exist" do
      @workset.allow_review(@author)

      @workset.review_capabilities.first.revision == @workset.object.revision
    end

    it "should create only one review capability for a particular revision" do
      @workset.allow_review(@author)
      @workset.allow_review(@collborator)

      @workset.review_capabilities.length.must_equal 1
    end

    it "should create one review capability for each unique revision" do
      @workset.allow_review(@author)
      @workset.revision = "NEW_REVISION"
      @workset.allow_review(@collborator)

      @workset.review_capabilities.length.must_equal 2
    end
  end

  describe "#can_view?" do
    before :each do
      object = Occam::Object.new(nil, :name => "workset", :objectInfo => {}, :private => 1)
      object.save
      @private_workset = Occam::Workset.create(:occam_object_id => object.id)

      @private_workset.collaborators << @collaborator
      @private_workset.authors       << @author
    end

    it "should return true when the workset is public and given person is nil" do
      @workset.can_view?(nil).must_equal true
    end

    it "should return true when the workset is public and given person is not a collaborator nor author" do
      @workset.can_view?(@person).must_equal true
    end

    it "should return true when the workset is public and given person is a collaborator" do
      @workset.can_view?(@collaborator).must_equal true
    end

    it "should return true when the workset is public and given person is an author" do
      @workset.can_view?(@author).must_equal true
    end

    it "should return false when the workset is private and given person is nil" do
      @private_workset.can_view?(nil).must_equal false
    end

    it "should return false when the workset is private and given person is not a collaborator nor author" do
      @private_workset.can_view?(@person).must_equal false
    end

    it "should return true when the workset is private and given person is a collaborator" do
      @private_workset.can_view?(@collaborator).must_equal true
    end

    it "should return true when the workset is private and given person is an author" do
      @private_workset.can_view?(@author).must_equal true
    end
  end

  describe "#can_edit?" do
    it "should return false when given person is nil" do
      @workset.can_edit?(nil).must_equal false
    end

    it "should return false when given person is not a collaborator nor author" do
      @workset.can_edit?(@person).must_equal false
    end

    it "should return false when given person is a collaborator" do
      @workset.can_edit?(@collaborator).must_equal false
    end

    it "should return true when given person is an author" do
      @workset.can_edit?(@author).must_equal true
    end
  end

  describe "#update_info" do
    it "should invoke an occam worker with the given data" do
      Occam::Worker.expects(:perform).returns("")
      @workset.update_info({})
    end
  end

  describe "#fork" do
    it "should invoke an occam worker with the given name and person" do
      Occam::Worker.expects(:perform).returns("")
      @workset.fork("foo", @person)
    end
  end

  describe "#addGroup" do
    it "should invoke an occam worker with the given name" do
      Occam::Worker.expects(:perform).returns("")
      @workset.addGroup("foo")
    end
  end

  describe "#addExperiment" do
    it "should invoke an occam worker with the given name" do
      Occam::Worker.expects(:perform).returns("")
      @workset.addExperiment("foo")
    end
  end

  describe "#addObject" do
    it "should invoke an occam worker with the given name" do
      Occam::Worker.expects(:perform).returns("")
      @workset.addObject("foo")
    end
  end

  describe "#local_links" do
    before :each do
      @local_link = Occam::LocalLink.create(:person_id => @person.id,
                                            :workset_id => @workset.id)
    end

    it "should return an empty array when person is nil" do
      @workset.local_links(nil).must_equal []
    end

    it "should return the local links that are associated with the given person" do
      @workset.local_links(@person).first.id.must_equal @local_link.id
    end

    it "should not return any local links that are associated with someone other than the given person" do
      @workset.local_links(@author).must_equal []
    end
  end
end
