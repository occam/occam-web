/*
 * This module handles the DataViewer widget which displays structured output
 * for viewing and manipulating with other widgets.
 */

// TODO: Pull Units out when data point is selected
// TODO: Callback for data point dropdown
// TODO: Work with Configuration to feed data point dropdown

var initOccamDataViewer = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents an element containing
   * structured data in OCCAM according to a schema.
   */
  var DataViewer = Occam.DataViewer = function(element) {
    this.targetPivots = [];
    this.targets = {};
    this.element = element;
    this.element.css({
      "position": "relative"
    });

    this.element.perfectScrollbar();

    this.schema = {};
    this.data = {};
    this.events = {};

    this.initializeContextMenu();
  };

  DataViewer.prototype.trigger = function(name) {
    if (this.events[name]) {
      this.events[name].call(this, {});
    };
    return this;
  };

  DataViewer.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  /*
   * This function applies events to the data rendered at the given element
   * for things such as collapse/expand and context menu.
   */
  DataViewer.bindEvents = function(element) {
    // Collapse Arrays
    element.find('ul.hash > li > span.expand').on('click', function(e) {
      $(this).toggleClass('shown');
      // Get associated array div
      if ($(this).hasClass('shown')) {
        $(this).parent().children('span.value').children('ul').show(200);
        $(this).text("\u25be");
      }
      else {
        $(this).parent().children('span.value').children('ul').hide(200);
        $(this).text("\u25b8");
      }
    });

    // Create a '...' div for collapsed data points which upon clicking
    // will expand the data once more.
    var fake_li = $('<li class="fake"><span class="key">...</span></li>').css({
      display: 'none'
    }).on('click', function(e) {
      $(this).parent().parent().children('span.expand').trigger('click');
    });

    element.find('ul.array > li.element > ul').append(fake_li);

    // Collapse array element hashes
    element.find('ul.array > li.element > span.expand').on('click', function() {
      $(this).toggleClass('shown');
      // Get associated array element div
      var thiz = $(this);
      if ($(this).hasClass('shown')) {
        $(this).parent().children('ul').children('li.fake').hide(100, function() {
          thiz.parent().children('ul').children('li:not(.fake)').show(150);
        });
        $(this).text("\u25be");
      }
      else {
        $(this).parent().children('ul').children('li:not(.fake)').hide(150, function() {
          thiz.parent().children('ul').children('li.fake').show(100);
        });
        $(this).text("\u25b8");
      }
    });
  };

  /*
   * This function will add a datapoint target for the context menu. For
   * example, you might add a "Group" target which when selected in the menu
   * will add the datapoint to that group.
   */
  DataViewer.prototype.addTarget = function(name, key, configuration, nesting) {
    if (this.targets[name] === undefined) {
      this.targetPivots.push(name);
      this.targets[name] = []
    }

    this.targets[name].push({
      name:          name,
      key:           key,
      configuration: configuration,
      nesting:       nesting
    });
  };

  /*
   * This function will create and initialize the context menu for the
   * DataViewer.
   */
  DataViewer.prototype.initializeContextMenu = function() {
    var self = this;

    self.menu = $('<div id="graph-builder-menu"></div>').css({
      display: 'none',
      position: 'absolute',
      "z-index": '999'
    }).on('mousedown', function(event) {
      event.stopPropagation();
    });

    $('body').on('mousedown', function(event) {
      self.menu.css({
        display: 'none'
      });
    }).append(self.menu);

    $(window).on('blur', function(event) {
      self.menu.css({
        //display: 'none'
      });
    });

    // Add structure to the context menu
    self.menu.append('<h3>Across:</h3>');
    self.menu.append('<div class="select pivot"><select></select></div>');
    self.menu.append('<br>');
    self.menu.append('<h3>Each:</h3>');
    self.menu.append('<div class="select each"><select></select></div>');
    self.menu.append('<br>');
    self.menu.append('<h3>Pick:</h3>');
    self.menu.append('<div class="select key"><select></select></div>');
    self.menu.append('<br>');
    self.menu.append('<h3>Append To:</h3>');
    self.menu.append('<div class="select data-pivot"><select></select></div>');
    self.menu.append('<br>');
    self.menu.append('<h3>In:</h3>');
    self.menu.append('<div class="select data-key"><select></select></div>');
    self.menu.append('<br>');
    self.menu.append('<input type="button" class="button" value="Append"></input>');
  };

  DataViewer.prototype.attachContextMenu = function() {
    var self = this;
    var element = self.element;

    element.find('li span.key').css({
      'cursor': 'pointer'
    }).each(function() {
      $(this).on('click', function(event) {
        var offset = $(this).offset();
        var width  = $(this).width();
        var height = $(this).height();
        self.fillContextMenu($(this));

        // What key is this?
        // Do not open the context menu when someone is selecting text
        if (window.getSelection().rangeCount > 0 && !window.getSelection().getRangeAt(0).collapsed) {
          return;
        }

        if (self.contextMenuItemCount() == 0) {
          //return;
        }

        // Open context menu
        var x, y;
        x = offset.left + width;
        y = offset.top + height/2;
        // TODO: reposition if off screen
        self.menu.css({
          left:    x + 10,
          top:     y - 12,
          display: 'block'
        });

        event.preventDefault();
        event.stopPropagation();
      });
    });
  };

  /*
   * This method will render the data within the element while fitting it
   * and typesetting the contents.
   */
  DataViewer.prototype.render = function() {
    this.element.append($('<h2>Data</h2>'));
    renderImpl(this.element, data, schema);
  };

  /*
   * This method loads the given data and updates the bound element to display
   * that data.
   */
  DataViewer.prototype.load = function(ready) {
    var self = this;

    var object_id       = this.element.data('object-id');
    var object_revision = this.element.data('object-revision');

    // Load the data html
    this.element.load('/objects/' + object_id + '/' + object_revision + '/data', function() {
      DataViewer.bindEvents(self.element);
      self.attachContextMenu();

      ready.call(self);
    });

    return this;
  };

  /*
   * This helper method returns the given element's label.
   */
  DataViewer.labelFor = function(element) {
    return element.text();
  };

  /*
   * This helper method returns an array of all key elements for parents of the
   * given element which label arrays. The purpose of this method is to extract
   * all of the points in the data structure where one could iterate across.
   */
  DataViewer.arrayPivotsFor = function(element) {
    var arrayItems = element.parents('ul.array');
    var arrayItem  = arrayItems.parents('li');
    var arrayKeys  = arrayItem.children('span.key');

    return arrayKeys;
  };

  /*
   * This helper method will return an array for the nesting of this element.
   */
  DataViewer.nestingFor = function(element) {
    var elements = element.parentsUntil('li.tab-panel.results-data', 'li');
    return $.makeArray(elements.map(function() {
      var key = $(this).data('key');
      if ($(this).hasClass('element')) {
        key = parseInt(key);
      }
      else {
        key = atob(key);
      }
      return key;
    }));
  };

  /*
   * This function adds an item to the popup context menu. It will use the
   * given label for the text and the given callback will be fired when the
   * menu item is selected.
   */
  DataViewer.prototype.addContextMenuItem = function(label, callback) {
    var self = this;
    var b64_name = "";

    var menu_item = $("<li class='graph-builder-menu-item select' data-key='" + b64_name + "'>" + label + "</li>");
    this.menu.children('ul').append(menu_item);

    menu_item.on('click', function(event) {
      event.preventDefault();
      event.stopPropagation();

      self.menu.css({
        display: 'none'
      });

      if (callback !== undefined) {
        callback.call(self);
      }
    });
  };

  /*
   * This function returns the number of menu items available in the
   * context menu.
   */
  DataViewer.prototype.contextMenuItemCount = function() {
    return this.menu.children('ul').children('li').length;
  };

  /*
   * The function adds menu items to the context menu according to the data
   * point given.
   */
  DataViewer.prototype.fillContextMenu = function(element) {
    var self = this;

    // Add, for this data element, a set of menu items consisting of:
    // "Add <name> Of <array-pivot> To <target>
    //
    // Targets are attached to the data viewer via the addTarget function.
    // Array-Pivots are areas of the data that iterate which an individual may
    // use to select a range of data.
    // The name is the element itself.

    // Remove existing fields
    self.menu.find('option').remove();

    var keyName     = DataViewer.labelFor(element);
    var arrayPivots = DataViewer.arrayPivotsFor(element);
    var nesting     = DataViewer.nestingFor(element);

    var objectId       = self.element.data('object-id');
    var objectRevision = self.element.data('object-revision');

    var sourcePivotSelector = self.menu.find('.select.pivot select');
    var sourceEachSelector  = self.menu.find('.select.each select');
    var sourceKeySelector   = self.menu.find('.select.key select');

    var destPivotSelector = self.menu.find('.select.data-pivot select');
    var destKeySelector   = self.menu.find('.select.data-key select');

    sourceKeySelector.append('<option>' + keyName + '</option>');

    sourceEachSelector.on('change keydown', function(event) {
      var index = $(this).children(':selected').index();

      if (sourcePivotSelector.children(':selected').index() < index) {
        sourcePivotSelector.children('option').slice(index, index+1).prop('selected', true);
      }
    });

    self.targetPivots.forEach(function(pivot, i) {
      var option = $('<option>' + pivot + '</option>')
                      .data('pivot-index', i);
      destPivotSelector.append(option);
    });

    destPivotSelector.on('change keyup', function(event) {
      destKeySelector.find('option').remove();

      var i = parseInt($(this).find(':selected').data('pivot-index'));
      if (isNaN(i)) {
        i = 0;
      }

      var pivot = self.targetPivots[i];

      self.targets[pivot].forEach(function(target, i) {
        var option = $('<option>' + target.key + '</option>');
        option.data('target-pivot', pivot);
        option.data('target-index', i);

        destKeySelector.append(option);
      });
    }).trigger('change');

    // For every array, add a select across menu item
    sourcePivotSelector.append('<option>' + keyName + '</option>');
    sourceEachSelector.append('<option>' + keyName + '</option>');
    arrayPivots.each(function() {
      var pivotElement = $(this);
      var pivotName    = pivotElement.text();
      var option = $('<option>' + pivotName + '</option>');
      option.data('pivot-index', $(this).index());

      sourcePivotSelector.append(option);

      option = $('<option>' + pivotName + '</option>');
      option.data('pivot-index', $(this).index());
      sourceEachSelector.append(option);
    });

    if (arrayPivots.length > 0) {
      sourcePivotSelector.children('option').slice(1,2).prop('selected', true);
      sourceEachSelector.children('option').slice(1,2).prop('selected', true);
    }

    this.menu.find('.button').one('click', function(event) {
      event.preventDefault();
      event.stopPropagation();

      // Add DataPoint
      var selectedDestKey     = destKeySelector.children(':selected');
      var selectedDestPivot   = destPivotSelector.children(':selected');
      var selectedSourcePivot = sourcePivotSelector.children(':selected');
      var selectedSourceEach  = sourceEachSelector.children(':selected');
      var selectedSourceKey   = sourceKeySelector.children(':selected');

      var target_pivot  = selectedDestKey.data('target-pivot');
      var target_index  = parseInt(selectedDestKey.data('target-index'));
      var pivot_point   = parseInt(selectedSourcePivot.data('pivot-index'));
      var iterate_point = parseInt(selectedSourceEach.data('pivot-index'));

      var target = self.targets[target_pivot][target_index];
      console.log('Datapoint Append');
      console.log(selectedSourceKey);
      console.log(selectedSourceEach);
      console.log(selectedSourcePivot);
      console.log(selectedDestKey);
      console.log(selectedDestKey.data('target-index'));
      console.log(selectedDestKey.data('target-pivot'));
      console.log(selectedDestPivot);
      console.log(pivot_point);
      console.log(iterate_point);

      // Fire callback for data point selection
      if (selectedSourceKey.index() === selectedSourcePivot.index() && selectedSourcePivot.index() === selectedSourceKey.index()) {
        // Single data point
        console.log("Adding SINGLE data point for: ");
        console.log(nesting);
        target.configuration.addDataPoint(objectId, objectRevision, nesting, target.nesting);
      }
      else {
        // Multiple data points
        // TODO: handle only starting from pivotElement and adding a DataPoint object
        // for each item of iterateElement

        var pivotElement = arrayPivots.slice(pivot_point-1, pivot_point);
        var iterateElement = arrayPivots.slice(iterate_point-1, iterate_point);

        console.log("Adding data point for: ");
        console.log(target_pivot);
        console.log(target_index);
        console.log(target);
        console.log(pivotElement);
        console.log(iterateElement);

        // Copy the nesting
        var subNesting = nesting.slice();
        var j = 0;

        for (var i = 0; i < arrayPivots.length; i++) {
          var subPivotElement = arrayPivots.slice(i, i+1);
          while(j < subNesting.length && (typeof subNesting[j] != "number")) {
            j++;
          }

          // Add the whole range for this pivot point
          subNesting[j] = [
            [0, subPivotElement.next().children('ul').children('li').length - 1]
          ];

          // Once we hit the pivot point we selected, stop. Leave the rest
          // of the values alone.
          if (subPivotElement[0] == pivotElement[0]) {
            break;
          }
        }

        target.configuration.addDataPoint(objectId, objectRevision, subNesting, target.nesting);

        $('body').trigger('mousedown');
      }
    });
  };

  DataViewer.prototype.toggleSelectAcross = function(element, tab) {
    // Toggle class
    element.toggleClass("in-results");

    if (!element.hasClass("in-results")) {
      // Unmark all tags in data tab
      this.deselect(element);

      // Remove from data points list
      this.deselectAcross(element, tab);
    }
    else {
      // Mark tags in data tab
      this.select(element);

      // Add to date points list
      this.selectAcross(element, tab);
    }
  };

  DataViewer.prototype.deselectAcross = function(element, tab) {
    // Pull out the item's context
    var keys = collectKeys(element);
    var key_name = keys[0][0];

    // Could produce the key from element.parent().data('id')
    var key = "data";
    var normal_key = "";
    for(var i = keys.length - 1; i >= 0; i--) {
      key = key + "-" + keys[i][1];
      normal_key = normal_key + keys[i][0];
      if (i > 0) {
        normal_key = normal_key + ".";
      }
    }

    this.removeDataPoint(normal_key, key, tab);

    // Remove from graph
    options.data.groups.forEach(function(group, i) {
      if (group.key == element.data('key')) {
        options.data.groups.splice(i,1);
      }
    });

    //produceGraph();
  };

  DataViewer.prototype.deselectPoint = function(element) {
    // Pull out the item's context
    var keys = collectKeys(element);
    var key_name = keys[0][0];

    // Could produce the key from element.parent().data('id')
    var key = "data";
    var normal_key = "";
    for(var i = keys.length - 1; i >= 0; i--) {
      key = key + "-" + keys[i][1];
      normal_key = normal_key + keys[i][0];
      if (i > 0) {
        normal_key = normal_key + ".";
      }
    }

    this.removeDataPoint(normal_key, key, $('.data.active').index());

    // Remove from graph
    options.data.groups.forEach(function(group, i) {
      if (group.key == element.data('key')) {
        options.data.groups.splice(i,1);
      }
    });

    //produceGraph();
  };

  DataViewer.prototype.removeDataPoint = function(key, b64_key, tab) {
    this.element.find('ul.data-points li.data-point[data-key=' + b64_key + '][data-tab=' + tab +']').remove();
  };

  DataViewer.prototype.addDataPoint = function(element, key, b64_key, tab) {
    var new_li = $('<li class="object data-point" data-key="' + b64_key + '" data-tab="' + tab + '"><h2>' + key + '</h2><p class="remove"><a href="javascript:void(0)">Remove</a></p></li>');
    $('ul.data-points').append(new_li);

    new_li.find('a').on('click', function(event) {
      this.deselect(element);
      this.deselectPoint(element);
      this.removeDataPoint(key, b64_key, tab);
    });
  };

  DataViewer.prototype.selectAcross = function(element, tab) {
    // Pull out the item's context
    var keys = this.collectKeys(element);
    var key_name = keys[0][0];

    // Could produce the key from element.parent().data('id')
    var key = "data";
    var normal_key = "";
    for(var i = keys.length - 1; i >= 0; i--) {
      key = key + "-" + keys[i][1];
      normal_key = normal_key + keys[i][0];
      if (i > 0) {
        normal_key = normal_key + ".";
      }
    }

    this.addDataPoint(element, normal_key, key, tab);

    // TODO: Keys as base64 values
    // TODO: Tag key here as its full path (foo.bar.baz instead of just baz)
    // TODO: Apply each of these two to the deselect as well
    options.data.groups.push({
      name: element.data('key'),
      key: element.data('key'),
      series: this.getValues(element)
    });

    options.labels = options.labels || {};
    options.labels.y = element.parent().children('span.value').data('units');

    //produceGraph();
  };

  DataViewer.prototype.selectPoint = function(element) {
    // Pull out the item's context
    var keys = this.collectKeys(element);
    var key_name = keys[0][0];

    // Could produce the key from element.parent().data('id')
    var key = "data";
    var normal_key = "";
    for(var i = keys.length - 1; i >= 0; i--) {
      key = key + "-" + keys[i][1];
      normal_key = normal_key + keys[i][0];
      if (i > 0) {
        normal_key = normal_key + ".";
      }
    }

    this.addDataPoint(element, normal_key, key, $('.data.active').index());

    // TODO: Keys as base64 values
    // TODO: Tag key here as its full path (foo.bar.baz instead of just baz)
    // TODO: Apply each of these two to the deselect as well
    options.data.groups.push({
      name: element.data('key'),
      key: element.data('key'),
      series: [getValue(element)]
    });

    options.labels = options.labels || {};
    options.labels.y = element.parent().children('span.value').data('units');

    //produceGraph();
  };

  DataViewer.prototype.select = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    if (grandparent_ul.hasClass("array")) {
      // Highlight all
      grandparent_ul.children('li.element')
                    .children('ul')
                    .children('li')
                    .children('span.key').each(function() {
        if ($(this).data('key') == key) {
          $(this).addClass("in-results");
          $(this).css("color", "red");
        }
      });
      element.css("color", "red");
    }
    else {
      element.css("color", "blue");
    }
  };

  // Given a data 'span.key' element, this will return the keys used to
  // reach this value.
  DataViewer.prototype.collectKeys = function(element) {
    var keys = [];

    var current = element;
    for(;;) {
      var key = current.data('key');

      var base64_key = current.parent().data('id').split('-').pop();
      var hash_ul = current.parent().parent();
      var tmp_element = hash_ul.parent().parent();

      keys.push([key, base64_key]);

      // Break at root
      if (hash_ul.parent().hasClass('data')) {
        break;
      }

      if (tmp_element.hasClass('array')) {
        var array_element = hash_ul.parent();
        var array_index = array_element.data('id').split('-').pop();

        // Get array index
        keys.push(["array", parseInt(array_index)]);

        // Align to the key for this array
        current = tmp_element.parent().parent().children('.key');
      }
      else {
        // Normal hash, iterate on this key
        current = tmp_element.children('.key');
      }

      if (current.length == 0) {
        break;
      }
    }

    return keys;
  };

  DataViewer.prototype.getValues = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    var ret = [];

    // Find root

    if (grandparent_ul.hasClass("array")) {
      // Highlight all
      grandparent_ul.children('li.element')
                    .children('ul')
                    .children('li')
                    .children('span.key').each(function() {
        if ($(this).data('key') == key) {
          ret.push(parseFloat($(this).parent().children('span.value').text()));
        }
      });
    }

    return ret;
  };

  DataViewer.prototype.deselect = function(element) {
    var parent_ul = element.parent().parent();
    var grandparent_ul = parent_ul.parent().parent();

    var key = element.data('key');

    // Unhighlight all
    grandparent_ul.children('li.element')
                  .children('ul')
                  .children('li')
                  .children('span.key').each(function() {
      if ($(this).data('key') == key) {
        $(this).removeClass("in-results");
        $(this).css("color", "");
      }
    });
    element.css("color", "");
  };
};
