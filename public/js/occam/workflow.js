/* This file handles the interactivity of workflows.
 */

// Initialize any Workflows on the current page
$(function() {
  var workflows = $('.content .workflow');

  workflows.each(function() {
    var workflow = new Occam.Workflow($(this));
  });
});

var initOccamWorkflow = function(Occam) {
  var Workflow = Occam.Workflow = function(element) {
    this.element = element;

    var empty = this.element.find('.input.start').length == 1;

    // Add nice scrollbars to workflows
    element.perfectScrollbar();

    this.applyContainerEvents();
 //   this.removeInputBoxes();

    if (empty) {
     // this.element.find('.input-button').trigger('click');
    }
    else {
    }

    this.element.find('.plus').on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Open an input section
    });
  };

  /* This function is the event handler for when the "Attach" button is
   * clicked. It should add an "attach" event to the queue.
   */
  Workflow.prototype.submitAttach = function(newActiveBox, container, workflow) {
    // Obviously, name and type might be wrong when somebody types something
    // weird in after selecting an object from the dropdown.

    var form = newActiveBox.find('form');

    // Get the hidden field with the object id requested to add
    var objectId = form.find(".object-id").val().trim();
    var objectRevision = form.find(".object-revision").val().trim();
    var connectionIndex = form.find("input[name=connection_index]").val().trim();
    var objectType = form.find("input[name=object_type]").val().trim();
    var objectName = form.find("input[name=object_name]").val().trim();

    Occam.object.queuePushAttach(connectionIndex, objectId, objectRevision, function(success) {
      console.log("Attach finished: " + success);
    });

    // Generate the container
    this.spawnContainerFromInputBox(newActiveBox, container, objectId, objectRevision, connectionIndex, objectType, objectName);
  };

  /* This function will augment the given container to spawn input points or
   * spinners for when new nodes are added.
   */
  Workflow.prototype.spawnContainerFromInputBox = function(inputBox, container, objectId, objectRevision, connectionIndex, objectType, objectName) {
    var self = this;

    self.hideActiveBox(inputBox, true);

    // Update the workflow
    // TODO: sanitize the object type
    container.removeClass('input').children('.container-content')
                                  .addClass('icon')
                                  .addClass(objectType);

    // Add container popup events
    var content = container.children('.container-content');
    content.css({
      cursor: "pointer"
    }).on('click', function(event) {
      self.revealActiveBox($(this));
    });

    // Add remove button
    // TODO: NOTE: revisions might be incorrect?? because dynamic attaches won't
    //       know what the revision is ultimately
    var removeURL = "/worksets/" + Occam.object.workset.id + "/" + Occam.object.workset.revision
                  + "/experiments/" + Occam.object.id + "/" + Occam.object.revision
                  + "/connections/" + "0";
    // TODO: connection index
    var removeForm = $('<form name="remove_connection" method="post" action="' + removeURL + '"></form>');
    removeForm.append($('<input type="hidden" value="delete" name="_method"></input>'));
    removeForm.append($('<input class="delete" type="submit" value="detach"></input>'));
    content.append(removeForm);

    // Add Type
    content.append($('<h3 class="type">' + objectType + '</h3>'));

    // Add name
    content.append($('<h3 class="name">' + objectName + '</h3>'));

    // Add revision
    content.append($('<h3 class="revision">' + objectRevision.slice(0,7) + '</h3>'));

    // Add labels above the node
    container.append($('<div class="label type">' + objectType + '</div>'));
    container.append($('<div class="label name">' + objectName + '</div>'));
  };

  Workflow.prototype.createInputBox = function(newActiveBox, container, workflow) {
    var self = this;
    var objectTypeSelector = newActiveBox.find('form select.inputs');

    /* Object type dropdown*/
    var objectTypeDropdown = $('<ul></ul>');

    objectTypeDropdown.addClass('dropdown');
    objectTypeDropdown.css({
      "position": "fixed",
      "width": "198px",
      "display": "none",
      "height": "0px",
      "overflow": "auto",
      "background-color": "white",
      "border": "1px solid #aaa",
      "z-index": "99999"
    });

    $('body .content').append(objectTypeDropdown);
    newActiveBox.find('form input.button').attr('disabled', 'on')
                                          /*.on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      self.submitAttach(newActiveBox, container, workflow);
    }); //*/;

    var objectTypeInput = newActiveBox.find('form input.expandable');
    objectTypeInput.on('focus', function() {
      var dropdownItem = $('<li class="object"></li>');
      if ($(this).hasClass('object-type')) {
        /* Look up types and append type objects */
        $(this).on('keyup', function(event) {
          var input_type = $(this).parent().children('input.object-type').val();
          var url = '/search';

          $.post(url, {
            "search": input_type,
            "types":  "on"
          }, function(data) {
            objectTypeDropdown.children().remove();
            data["types"].forEach(function(object) {
              var item = dropdownItem.clone();
              item.append($('<h2 class="type"></h2>'));
              item.children('h2').text(object["object_type"]);
              item.children('h2').addClass('large-icon').addClass(object["object_type"]);

              item.on('click', function(event) {
                /* Set fields to reflect choice */
                newActiveBox.find('form input.object-type').val(object["object_type"]);
                newActiveBox.find('form input.object-name').val("");
                newActiveBox.find('form input.object-id').val("");
                newActiveBox.find('form input.button').attr('disabled', 'on');
                newActiveBox.find('form input.object-type').css({
                  "background-image": item.find('h2').css('background-image')
                });
                /* Set hidden field to: object['uid'] */
              });

              objectTypeDropdown.append(item);
            });

            objectTypeDropdown.append(dropdownItem.clone());
            objectTypeDropdown.append(dropdownItem.clone());
          }, 'json');
        }).trigger('keyup');
      }
      else {
        /* Look up objects via this type */
        $(this).on('keyup', function(event) {
          var input_type = $(this).parent().children('input.object-type').val();
          var input_name = $(this).val();

          var url = '/search';

          $.post(url, {
            "search": input_name,
            "type": input_type,
            "objects":  "on"
          }, function(data) {
            objectTypeDropdown.children().remove();

            var item = dropdownItem.clone();

            if (input_type != "") {
              item.append($('<h2 class="type"></h2><p></p>'));
              item.children('h2').text(input_type).addClass('large-icon').addClass(input_type);
              var created_name = "{new " + input_type + "}";
              item.children('p').text(created_name);

              item.on('click', function(event) {
                /* Set fields to reflect choice */
                newActiveBox.find('form input.object-name').val(created_name);
                /* Set hidden field to: "" to create a new object */
                newActiveBox.find('form input.object-id').val();
                newActiveBox.find('form input.button').removeAttr('disabled', '');
              });

              objectTypeDropdown.append(item);
            }

            data["objects"].forEach(function(object) {
              var item = dropdownItem.clone();
              item.append($('<h2 class="type"></h2><p></p>'));
              item.children('h2').text(object["object_type"]);
              item.children('h2').addClass('large-icon').addClass(object["object_type"]);
              item.children('p').text(object["name"]);

              item.on('click', function(event) {
                console.log(object);
                /* Set fields to reflect choice */
                newActiveBox.find('form input.object-type').val(object["object_type"]);
                newActiveBox.find('form input.object-name').val(object["name"]);
                /* Set hidden field to: object['uid'] */
                newActiveBox.find('form input.object-id').val(object["uid"]);
                newActiveBox.find('form input.object-revision').val(object["revision"]);
                newActiveBox.find('form input.button').removeAttr('disabled', '');

                newActiveBox.find('form input.object-type').css({
                  "background-image": item.find('h2').css('background-image')
                });
              });

              objectTypeDropdown.append(item);
            });
          }, 'json');
        }).trigger('keyup');
      }

      $(this).css({
        "position": "relative",
        "top":  "-1px",
        "margin-bottom": "-1px",
        "border": "1px solid #aaa",
      }).animate({
        "width": 188 - parseInt($(this).css("padding-left")) + "px",
        "left": "-40px",
        "background-color": "white"
      }, 200, function() {
        var offset = $(this).offset();
        objectTypeDropdown.css({
          "left": offset.left + "px",
          "top":  (offset.top + 41) + "px",
          "display": "block",
          "height": "0px"
        }).animate({
          "height": "100px"
        }, 200);
      });
    }).on('blur', function() {
      objectTypeDropdown.fadeOut(200);
      $(this).css({
      }).animate({
        "width": 110 - parseInt($(this).css("padding-left")) + "px",
        "left": "0px",
        "background-color": ""
      }, 200, function() {
        $(this).css({
          "border": "",
          "top":  "",
          "margin-bottom": "",
        })
      });
    });

    /* Object type dropdown */
    objectTypeSelector.on('change', function() {
      var selected = $(this).children(':selected');

      /* Fill out the object selection dropdown with appropriate objects that
       * are of the specified type.
       */
      var object_id  = $(this).data('object-id');
      var thiz = $(this);
      var input_type = selected.attr('value');

      if (object_id === undefined) {
        // This is the global object. Grab ALL object types and inputs.
        var url = '/objects?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var objectSelector = thiz.parent().children("select.objects");
          objectSelector.children().remove();
          data.forEach(function(object) {
            var option = $('<option></option>');
            option.text(object["name"]);
            option.attr('value', object["id"]);
            option.data('object-id', object["id"]);
            option.data('object-revision', object["revision"]);
            objectSelector.append(option);
          });
        });
      }
      else {
        var url = '/objects/' + object_id + '/inputs?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var objectSelector = thiz.parent().children("select.objects");
          objectSelector.children().remove();
          data.forEach(function(input) {
            input["objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              option.data('object-revision', object["revision"]);
              objectSelector.append(option);
            });

            input["indirect_objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              option.data('object-revision', object["revision"]);
              objectSelector.append(option);
            });
          });
        });
      }
    });
  };

  Workflow.prototype.createContextMenu = function(newActiveBox, container, workflow) {
    var boxContent = newActiveBox.children('.container-content');

    var removeMenuItem = boxContent.find('li.remove');
    var removeForm = container.children('.container-content').children('form[name=remove_connection]');

    removeMenuItem.on('click', function(event) {
      // Submit delete form
      // TODO: ajax
      removeForm.submit();
      newActiveBox.trigger('blur');

      event.stopPropagation();
      event.preventDefault();
    });

    var connectionIndex = parseInt(container.data('connection-index'));
    var configureMenuItem = boxContent.find('li.configure');
    configureMenuItem.on('click', function(event) {
      // Determine the configuration tab
      newActiveBox.trigger('blur');

      // Get the configuration card
      var configurationCard = workflow.parent().next();
      console.log(configurationCard);
      var configurationTab =
        configurationCard.find('ul.tabs.configuration-object > li.tab[data-connection-index=' + connectionIndex + ']');

      console.log(configurationTab);

      configurationTab.trigger('click');

      event.stopPropagation();
      event.preventDefault();
    });

    // Assign the object's url to the "View" menu option.
    var viewMenuItem = boxContent.find('li.view');
    var objectUrl = container.find('h3.name a').attr('href');
    viewMenuItem.on('click', function(event) {
      // Open the object in a new window
      window.open(objectUrl);
      console.log(objectUrl);

      newActiveBox.trigger('blur');

      event.stopPropagation();
      event.preventDefault();
    });

    // Amend the "View" menu option to display the object's revision.
    var revision = "@" + container.find('h3.revision').text().trim();
    viewMenuItem.find('.revision').text(revision);
  };

  /* This helper function will reveal the active box when the given element
   * is to be revealed in full.
   */
  Workflow.prototype.revealActiveBox = function(containerContent) {
    var self = this;

    var container = containerContent.parent();
    var workflow = containerContent.parents('.workflow.fitted');
    var position = containerContent.offset();
    var workflowOffset = workflow.offset();
    var isInput = containerContent.parent().hasClass('input');

    var x = position.left - workflowOffset.left + workflow.scrollLeft();
    var y = position.top  - workflowOffset.top  + workflow.scrollTop();

    var activeBox    = workflow.children('.container.embedded.mold');
    var newActiveBox = activeBox.clone();
    newActiveBox.removeClass('mold');

    var boxContent = newActiveBox.children('.container-content');

    /* When it is an input box, add all of the children since it contains
     * the entire form we want to still present.
     *
     * Otherwise, remove everything and create the context menu.
     */
    if (isInput) {
      newActiveBox.addClass('input');
      boxContent.empty();
      boxContent.append(containerContent.children().clone());

      this.createInputBox(newActiveBox, container, workflow);
    }
    else {
      this.createContextMenu(newActiveBox, container, workflow);
    }

    newActiveBox.attr('data-left', x);
    newActiveBox.attr('data-top',  y);

    workflow.append(newActiveBox);

    /* Embedded container fits its contents */
    newActiveBox.css({
      left:       x - 1,
      top:        y - 1,
      visibility: "visible"
    }).children('.container-content').css({
      left: "0px",
      top:  "0px"
    }).children().css({
      visibility: "visible",
      opacity:    0.0
    });

    newActiveBox.focus();

    /* Reuse the border color */
    newActiveBox.children('.container-content').css({
      "border-color": containerContent.css("border-left-color")
    });

    /* Animate box expanding */
    newActiveBox.stop(true).animate({
      left: x - 46,
      top:  y - 46
    }, 200)
      /* Expand to this size: */
      .children('.container-content').stop(true).animate({
        width:   "120px",
        height:  "120px",
        opacity: 1.0
      }, 200)
        /* Ensure that children are eventually visible (because they look rather
         * silly as they are expanding and as they fill the space with respect to
         * css overflow) */
        .children().stop(true).animate({
          opacity: 1.0
        }, 200);

    // Collapse when it is no longer in focus
    if (!isInput) {
      newActiveBox.on('blur', function(event) {
        self.hideActiveBox($(this));
      });
    }
  };

  Workflow.prototype.hideActiveBox = function(newActiveBox, force) {
    if (force === undefined) {
      force = false;
    }

    // Do not collapse input containers
    if (!force && newActiveBox.hasClass('input')) {
      return;
    }

    // Determine old location and animate:
    var x = parseFloat(newActiveBox.attr('data-left'));
    var y = parseFloat(newActiveBox.attr('data-top'));

    newActiveBox.stop(true).animate({
      left: x - 1,
      top:  y - 1,
    }, 200, "swing", function() {
      // Destroy the box
      newActiveBox.remove();
    });

    // Shrink to this size:
    newActiveBox.children('.container-content').stop(true).animate({
      height: "30px",
      width:  "30px"
    }, 200);

    // Slowly hide children because, like before, they look silly as they
    // shrink down due to overflow oddness.
    newActiveBox.children('.container-content').children().stop(true).animate({
      "opacity": 0.0
    }, 200, "swing", function() {
    });
  };

  /* This function will attach events to each workflow object to reveal the
   * options menu for each connection. This menu is used to alter the workflow.
   * Such as deleting the node, quickly going to the configuration of the node,
   * or viewing the object at the revision specified by the workflow.
   */
  Workflow.prototype.applyContainerEvents = function(containers) {
    var self = this;
    if (containers === undefined) {
      containers = this.element
        .find('.container:not(.embedded) .container-content');
    }

    // Reveal box on click
    containers.css({
      cursor: "pointer"
    }).on('click', function(event) {
      self.revealActiveBox($(this));
    });
  };

  /* This function will remove any input boxes that are visible and replace
   * them with + buttons. These are only visible when javascript is not being
   * used and thus for accessibility.
   */
  Workflow.prototype.removeInputBoxes = function() {
    var self = this;

    var input_containers = [];
    this.element.find('.object.input').each(function() {
      /* Retain input container */
      var container = $(this).find('.container');
      container.css({
        "height": "0px"
      }).removeClass('wide');

      input_containers[input_containers.length] = {
        "container": $(this),
        "parent":    $(this).parent()
      };

      /* Remove input container from DOM */
      $(this).remove();
    });

    /* Add input button to re-add input container to DOM */
    input_containers.forEach(function(e, i) {
      var add_button = $('<div></div>');
      add_button.addClass("input-button");

      var last_container = e.parent.children("li:last-child")
                                   .children('.row')
                                   .children('.info')
                                   .children('.container');
      var position = last_container.offset();
      var workflow_offset = self.element.offset();
      var appending = true;
      var x = 0;
      var y = 0;
      var bottom = 0;
      if (last_container.length == 0) {
        /* This is adding the first input for a node */
        /* In this case, we want a (+) bubble directly to the
         * left of the existing node. */
        last_container = e.parent;
        position = e.parent.offset();
        x = position.left - workflow_offset.left + 39;
        y = position.top  - workflow_offset.top;

        bottom = (e.parent.height() - 15) / 2;

        appending = false;
      }
      else {
        x = position.left - workflow_offset.left + 39;
        y = position.top  - workflow_offset.top  + 77;
        bottom = 8;
      }

      add_button.css({
        "bottom": bottom,
      })
      .on('mouseenter', function(e) {
        // Pulse the color

        $(this).animate({
          "background-color": "hsl(120, 50%, 50%)"
        }, 200);
        $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
          "border-color": "hsl(120, 50%, 50%)"
        }, 200);
      }).on('mouseout', function(event) {
        $(this).stop(true).animate({
          "background-color": "hsl(120, 50%, 80%)"
        }, 200);
        $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
          "border-color": "hsl(180, 40%, 90%)"
        }, 200);
      })
      .one('click', function(event) {
        /* Add input container element to line DOM */

        $(this).unbind('mouseout mouseenter click');
        $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
          "opacity": "0.0"
        }, 200, "swing", function() {
          $(this).remove();
        });
        $(this).stop(true).animate({
          "opacity": "0.0"
        }, 200, "swing", function() {
          $(this).remove();
        });

        e.parent.append(e.container);

        e.container.find('.container').animate({
          "height": "100px"
        }, 200);

        e.container.find('.container-content')
         .on('blah', function(event) {
           self.revealActiveBox($(this));
         })
         .trigger('blah');
      });

      // Dashes
      var add_button_dashes = $('<div></div>');
      add_button_dashes.addClass("input-dashes");

      if (!appending) {
        add_button_dashes.css({
          "border-top": "1px solid hsl(180, 40%, 90%)",
          "bottom": 16,
        });
      }
      else {
        add_button_dashes.css({
          "bottom": 14,
          "border-right": "5px solid hsl(180, 40%, 90%)",
          "border-bottom": "1px solid hsl(180, 40%, 90%)",
          "border-bottom-right-radius": "10px",
        });
      }

      add_button.attr("data-input-index", i);
      add_button_dashes.attr("data-input-index", i);

      last_container.append(add_button);
      last_container.append(add_button_dashes);
    });
  };
};
