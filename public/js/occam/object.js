/* This file maintains the Occam.Object class which handles functionality
 * related to Occam Objects. This class can pull down information and metadata
 * and post updates to Occam, if that is allowed by the object.
 */

/* Objects have queues of pending actions which need to be ACK'd before
 * continuing. That way actions are invoked in the correct order. An object's
 * revision is updated whenever an action is acknowledged. Some actions
 * require an object to be up-to-date in the backend worker, and thus must wait
 * until the queue is empty. For instance, running the object in the browser.
 * Basically, these actions are just in the queue as well... but we should
 * somehow indicate that the actions are delayed.
 */

// Initialize the current object
$(function() {
  // Main object for the page
  Occam.object = new Occam.Object();
});

var initOccamObject = function(Occam) {
  var Object = Occam.Object = function(id, revision) {
    // If no uuid/revision are given, pull out the object represented on the
    // page (if any)
    if (id === undefined) {
      this.element = $('body > .content > h1');

      this.id = this.element.find('#object-id').text().trim();
      this.revision = this.element.find('#object-revision').text().trim();

      // Discover the workset object
      this.workset = null;
      var worksetIdElement = this.element.find('#workset-id');

      if (worksetIdElement.length > 0) {
        var worksetId = worksetIdElement.text().trim();
        var worksetRevision = this.element.find('#workset-revision').text().trim();
        this.workset = new Occam.Object(worksetId, worksetRevision);
      }

      // Initialize any object viewers
      var objectViewer = $('.card.object-viewer');

      if (objectViewer.length > 0) {
        this.applyConfigurationEvents(objectViewer);
      }

      if (this.workset !== null) {
        console.log("Looking at object " + this.id + " @" + this.revision + " within " + this.workset.id + " @" + this.workset.revision);
      }

      // Initialize the Runner

      // Build tab

      var element = $('.card.terminal .terminal#build-terminal');

      if (element.length > 0) {
        var object_id        = element.data('object-id');
        var object_revision  = element.data('object-revision');
        var workset_id       = element.data('workset-id');
        var workset_revision = element.data('workset-revision');

        var buildTerminal = new window.Occam.Terminal("build", "tty", {
          "object_id":        object_id,
          "object_revision":  object_revision,
          "workset_id":       workset_id,
          "workset_revision": workset_revision
        }, element);
      }

      // Console tab

      element = $('.card.terminal .terminal#console-terminal');

      if (element.length > 0) {
        var object_id        = element.data('object-id');
        var object_revision  = element.data('object-revision');
        var workset_id       = element.data('workset-id');
        var workset_revision = element.data('workset-revision');

        var consoleTerminal = new window.Occam.Terminal("console", "tty", {
          "object_id":        object_id,
          "object_revision":  object_revision,
          "workset_id":       workset_id,
          "workset_revision": workset_revision
        }, element);
      }

      // Run tab

      element = $('.card.terminal .terminal#run-terminal');

      if (element.length > 0) {
        var object_id        = element.data('object-id');
        var object_revision  = element.data('object-revision');
        var workset_id       = element.data('workset-id');
        var workset_revision = element.data('workset-revision');
        var input_id         = element.data('input-id');
        var input_revision   = element.data('input-revision');

        var runTerminal = new window.Occam.Terminal("run", "log", {
          "object_id":        object_id,
          "object_revision":  object_revision,
          "workset_id":       workset_id,
          "workset_revision": workset_revision,
          "input_id":         input_id,
          "input_revision":   input_revision,
        }, element);
      }
    }
    else {
      // This is just an object abstraction
      this.id = id;
      this.revision = revision;

      // It is not represented in the DOM
      this.element = null;
    }

    this.queue = [];
    this.pending = 0;
    this.queueLock = false;

    return this;
  };

  /* This constant sets the number of preview panes that can be loading at a
   * time. This will help limit the load on the server and client when loading
   * a whole page of widgets.
   */
  Object.MAX_CONCURRENT_PREVIEW_LOADS = 8;

  /* This is the amount of time in milliseconds to wait for a widget to give
   * a "loaded" event. We will remove the progress indicator and allow
   * interaction only when receiving that message. Otherwise, after the
   * timeout, we will display an error notification.
   */
  Object.PREVIEW_TIMEOUT = 10000;

  /* This method yields the object viewer for the given object if it exists.
   */
  Object.prototype.viewer = function() {
  };

  /* This method adds an append event to the queue. When an empty value is
   * given, this will remove the key from the object metadata.
   */
  Object.prototype.queuePushSet = function(key, value, callback) {
    var args = [];

    args.push({ "values": [key] });

    if (value !== undefined) {
      args.push({ "values": [JSON.stringify(value)] });
    }

    args.push({ "key": "--input-type",
             "values": ['json'] });

    this.queuePush({
      "command": "set",
      "arguments": args
    }, callback);

    return this;
  }

  /* This method adds an append event to the queue. The optional 'at' field
   * will determine the index it will push the new item.
   */
  Object.prototype.queuePushAppend = function(key, value, at, callback) {
    var args = [];

    args.push({ "values": [key] });
    args.push({ "values": [JSON.stringify(value)] });

    if (at !== undefined && at !== null) {
      args.push({ "key": "--at",
               "values": [""+at] });
    }

    args.push({ "key": "--input-type",
             "values": ['json'] });

    this.queuePush({
      "command": "append",
      "arguments": args
    }, callback);

    return this;
  }

  /* This method adds an attach event to the queue.
   */
  Object.prototype.queuePushAttach = function(connection_index, object_id, object_revision, callback) {
    var args = [];

    if (connection_index >= 0) {
      args.push({
        "values": [connection_index.toString()]
      });
    }

    args.push({ "key": "--id",
             "values": [object_id] });
    args.push({ "key": "--object-revision",
             "values": [object_revision] });

    this.queuePush({
      "command": "attach",
      "arguments": args
    }, callback);

    return this;
  };

  Object.prototype.queuePushDetach = function(connection_index, callback) {
    this.queuePush({
      "command": "detach",
      "arguments": [
        { "values": [connection_index.toString()] }
      ]
    }, callback);

    return this;
  };

  /* This method adds an event to the queue.
   */
  Object.prototype.queuePush = function(command, callback) {
    this.queue.push([command, callback]);

    // ok. so when we have a command, issue it, and then have the ack of the
    // command issue the next one in sequence while firing any callback.
    // when there is no command, then just stop

    if (this.queueLock == false) {
      this.queueIssue();
    }

    return this;
  };

  /* This method yields the queue size.
   */
  Object.prototype.queueCount = function() {
    return this.queue.length;
  };

  /* This method, which is generally called internally and not meant to be
   * used externally, will invoke the next queued command.
   */
  Object.prototype.queueIssue = function() {
    var self = this;

    if (this.queue.length == 0) {
      return this;
    }

    // Lock queue
    self.queueLock = true;

    // Pull next action
    var queueItem = this.queue[0];
    var action    = queueItem[0];
    var callback  = queueItem[1];

    // Truncate queue
    self.queue = self.queue.slice(1);
    self.pending += 1;

    // Form action url
    var url = "/worksets/" + this.workset.id + "/" + this.workset.revision +
              "/objects/"  + this.id    + "/" + this.revision + "/history";

    // Reveal 'saving...' box
    $('.content h1 .saving').attr('aria-hidden', 'false');

    // Perform action and issue another command when it is successful
    $.post(url, JSON.stringify(action), function(revisions) {
      var revision = revisions['objectRevision'];
      var worksetRevision = revisions['worksetRevision'];
      var worksetId = self.workset.id;
      self.workset = new Occam.Object(worksetId, worksetRevision);

      self.revision = revision;

      // Call the callback once completed successfully
      if (callback !== undefined) {
        callback.call(self, true);
      }
    }, 'json').fail(function(error) {
      console.log("error??");
      $('.content h1 .saving.flash').attr('aria-hidden', 'true');
      $('.content h1 .error.flash').attr('aria-hidden', 'false');

      // Call the callback and indicate a failure
      if (callback !== undefined) {
        callback.call(self, false);
      }

      // TODO: we should flush the queue and try to react to failures
      //       everywhere we issue them. All flushed queue items should
      //       have their callbacks issued in failure.
      self.pending = 0;
    }).always(function() {
      // Issue another command
      self.pending -= 1;
      if (self.pending == 0 && self.queue.length == 0) {
        $('.content h1 .saving').attr('aria-hidden', 'true');
      }

      // Unlock queue
      if (self.pending == 0) {
        self.queueLock = false;
      }

      self.queueIssue();
    });

    return this;
  };

  /* This method retrieves the object info for this object.
   */
  Object.prototype.objectInfo = function(callback) {
    var self = this;

    if (this._objectInfo !== undefined) {
      // Pull result from cache
      callback(this._objectInfo);
    }
    else {
      var objectURL = "/objects/" + this.id + "/" + this.revision;
      $.getJSON(objectURL, function(data) {
        // Cache result
        self._objectInfo = data;
        callback(data);
      })
    }

    return this;
  };

  /*
   * This method is for objects with View tabs with configurations available.
   * These types of objects (for instance, our graph widgets) can be viewed
   * and played with interactively.
   */
  Object.prototype.applyConfigurationEvents = function(viewerElements) {
    var self = this;

    viewerElements.each(function() {
      var viewerElement = $(this);
      var preview = viewerElement.hasClass('preview');

      var load = function() {
        viewerElement.attr('data-status', 'loading');

        // Is this a preview?
        var configurationData = {};

        var iframe = viewerElement.find('iframe');

        // Previews don't load the viewer widget until they load their input
        if (preview) {
          timer = window.setTimeout(function() {
            error();
          }, Object.PREVIEW_TIMEOUT);

          iframe.data('fail-timer-id', timer);
          iframe[0].src = iframe.data('src');
          var loader = iframe.parent().children('.loading');
        }

        var inputObject = self;
        if (iframe.data('input-object-id') !== undefined) {
          inputObject = new Object(iframe.data('input-object-id'), iframe.data('input-object-revision'));
        }

        if (!preview) {
          var configurationCard = viewerElement.next().next();

          var configurationTabs = new Occam.Tabs(configurationCard.children('ul.tabs'));

          var viewerObjectLabel = configurationCard.find('.widget-object > a');
          var associationForm = configurationCard.find('form.inline.association');

          var optionsOpenLink = configurationCard.find('a.open-options');
          optionsOpenLink.on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).attr('aria-hidden', 'true');
            $(this).next().attr('aria-hidden', 'false');

            configurationCard.children('ul.tab-panels').attr('aria-hidden', 'false');
            configurationCard.children('ul.tabs').attr('aria-hidden', 'false');
          });

          var optionsCloseLink = configurationCard.find('a.close-options');
          optionsCloseLink.on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).attr('aria-hidden', 'true');
            $(this).prev().attr('aria-hidden', 'false');

            configurationCard.children('ul.tab-panels').attr('aria-hidden', 'true');
            configurationCard.children('ul.tabs').attr('aria-hidden', 'true');
          });

          configurationCard.find('form.widget-selection input.button').on('click', function(event) {
            event.stopPropagation();
            event.preventDefault();

            // Remove old configuration tabs
            while(configurationTabs.tabCount() > 2) {
              configurationTabs.removeTab(3);
            }

            var object_id = $(this).parent().children('input.hidden[name=object-id]').val();

            var objectURL = "/objects/" + object_id;

            $.getJSON(objectURL, function(data) {
              var object_revision = data['revision'];

              var viewPortal = viewerElement.first();

              viewerObjectLabel.text(data['name']);
              viewerObjectLabel.parent().attr('class', 'large-icon widget-object').addClass(data['type']);
              associationForm.find('input[name=object_id]').val(object_id);

              if (data['architecture'] == 'html') {
                viewerElements.each(function() {
                  if ($(this).hasClass('terminal')) {
                    $(this).attr('aria-hidden', 'true');
                  }
                  else {
                    $(this).attr('aria-hidden', 'false');
                    viewPortal = $(this);
                  }
                });

                iframe.data('object-id', object_id);
                iframe.data('object-revision', object_revision);
                iframe.data('object-name', data['name']);
                iframe.data('object-type', data['type']);

                //self.createWidgetConfigurationTabs(iframe, data, configurationTabs, object_id, object_revision);

                var widgetURL = "/objects/" + object_id + "/" + object_revision + "/raw/" + data["file"];
                iframe.attr('src', widgetURL);
              }
              else {
                viewerElements.each(function() {
                  if ($(this).hasClass('terminal')) {
                    $(this).attr('aria-hidden', 'false');
                    viewPortal = $(this);
                  }
                  else {
                    $(this).attr('aria-hidden', 'true');
                  }
                });

                // Update the terminal settings
                var terminal = viewPortal.find('.run-terminal .terminal');
                terminal.data('object-id', object_id);
                terminal.data('object-revision', object_revision);

                var input_id         = terminal.data('input-id');
                var input_revision   = terminal.data('input-revision');

                // Retrieve the terminal object
                var runTerminal = Occam.Terminal.retrieve(terminal);

                // Update the data message
                runTerminal.data = {
                  "object_id":        object_id,
                  "object_revision":  object_revision,
                  "input_id":         input_id,
                  "input_revision":   input_revision,
                };
              }

              // Post to the recently used list
              var currentPerson = $('#username a');
              var recentlyUsedURL = currentPerson.attr('href') + "/recentlyUsed";
              $.post(recentlyUsedURL, JSON.stringify({
                "object_id": object_id,
                "object_revision": object_revision
              }), function(data) {
              }, 'json');
            });
          });

          configurationCard.find('ul.object-list.recently-used').children('li').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();

            var objectId     = $(this).children('input[name=object-id]').val().trim();
            var attachButton = configurationCard.find('form.widget-selection input.button');
            var selectorName = attachButton.parent().children('input[name=name]');
            selectorName.val($(this).children('p').text().trim());
            var selectorId   = attachButton.parent().children('input.hidden[name=object-id]');
            selectorId.val(objectId);

            attachButton.trigger('click');
          });

          configurationCard.find('li.tab-panel.configuration').each(function() {
            Occam.Configuration.load($(this), function(configuration) {

              configurationData[configuration.label] = configuration.data();

              configuration.on('change', function(event) {
                configurationData[configuration.label] = event;

                iframe[0].contentWindow.postMessage({
                  name: 'updateConfiguration',
                  data: configurationData
                }, '*');
              });
            });
          });
        }

        if (!viewerElement.hasClass('terminal')) {
          window.addEventListener('message', function(event) {
            var message = event.data;

            // React if the source of the message is the iframe
            if (event.source === iframe[0].contentWindow) {
              var objectId       = self.id;
              var objectRevision = self.revision;

              if (message.name === 'updateData') {
                var url = "/objects/" + objectId +
                          "/"         + objectRevision +
                          "/configurations/data";
                $.post(url, JSON.stringify({
                  "object_id":       objectId,
                  "object_revision": objectRevision,
                }), function(data) {
                });
              }
              else if (message.name === 'loaded') {
                // The widget is telling us it has finished loading
                timer = iframe.data('fail-timer-id');
                window.clearTimeout(timer);
                iframe.parent().children('.loading').animate({
                  'opacity': 0.0
                }, 300, function() { $(this).remove() });
                finish();
              }
              else if (message.name === 'error') {
                // The widget is telling us it has finished loading
                error();
              }
              else if (message.name === 'updateConfiguration') {
                // Resend configuration data
                message['data'] = configurationData;
                iframe[0].contentWindow.postMessage(message, '*');
              }
              else if (message.name === 'updateInput') {
                // Resend input data
                inputObject.objectInfo(function(info) {
                  message['data'] = info;
                  if (preview) {
                    message['preview'] = true;
                  }
                  iframe[0].contentWindow.postMessage(message, '*');
                });
              }
            }
          });
        }

        // Resend input data
        inputObject.objectInfo(function(info) {
          message = {};
          message['name'] = 'updateInput';
          message['data'] = info;
          if (preview) {
            message['preview'] = true;
          }
          iframe[0].contentWindow.postMessage(message, '*');
        });
      };

      var pull = function() {
        var previewList = viewerElement.parents('ul.object-preview-list');
        var loadingCount = previewList.find('.object-viewer[data-status=loading]').length;
        if (loadingCount < Object.MAX_CONCURRENT_PREVIEW_LOADS) {
          viewerElement = previewList.find('.object-viewer[data-status=queued]').slice(0,1);
          if (viewerElement.length > 0) {
            load();
          }
        }
      };

      var queue = function() {
        viewerElement.attr('data-status', 'queued');
        pull();
      };

      var error = function() {
        var iframe = viewerElement.find('iframe');
        var loader = iframe.parent().children('.loading');
        loader.addClass('failed');
        iframe[0].src = "about:blank";
        finish();
      };

      var finish = function() {
        viewerElement.attr('data-status', 'done');
        if (preview) {
          pull();
        }
      };

      if (preview) {
        var iframe = viewerElement.find('iframe');
        var loader = $('<div class="loading"></div>');

        var iframeDimensions = {
          'width': viewerElement.innerWidth(),
          'height': viewerElement.innerHeight()
        };

        loader.css({
          'width': iframeDimensions.width,
          'height': iframeDimensions.height,
          'position': 'absolute',
          'top': 0,
          'left': 0,
        });

        iframe.parent().append(loader);
        viewerElement.appear();
        viewerElement.one('appear', queue);
      }
      else {
        load();
      }
    });
  };
}
