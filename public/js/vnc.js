"use strict"
var INCLUDE_URI = '/js/noVNC/';

$(function() {
  Util.load_scripts([
      "webutil.js", "base64.js", "websock.js", "des.js",
      "keysymdef.js", "keyboard.js", "input.js", "display.js",
      "inflator.js", "rfb.js", "keysym.js"
  ]);

  var rfb;
  var resizeTimeout;

  function UIresize() {
    if (WebUtil.getQueryVar('resize', false)) {
      var innerW = window.innerWidth;
      var innerH = window.innerHeight;
      var controlbarH = $D('noVNC_status_bar').offsetHeight;
      var padding = 5;
      if (innerW !== undefined && innerH !== undefined)
        rfb.setDesktopSize(innerW, innerH - controlbarH - padding);
    }
  }

  function FBUComplete(rfb, fbu) {
    UIresize();
    rfb.set_onFBUComplete(function() { });
  }

  function passwordRequired(rfb) {
    var msg;
    msg = '<form onsubmit="return setPassword();"';
    msg += '  style="margin-bottom: 0px">';
    msg += 'Password Required: ';
    msg += '<input type=password size=10 id="password_input" class="noVNC_status">';
    msg += '<\/form>';
    $D('noVNC_status_bar').setAttribute("class", "noVNC_status_warn");
    $D('noVNC_status').innerHTML = msg;
  }

  function setPassword() {
    rfb.sendPassword($D('password_input').value);
    return false;
  }

  function sendCtrlAltDel() {
    rfb.sendCtrlAltDel();
    return false;
  }

  function xvpShutdown() {
    rfb.xvpShutdown();
    return false;
  }

  function xvpReboot() {
    rfb.xvpReboot();
    return false;
  }

  function xvpReset() {
    rfb.xvpReset();
    return false;
  }

  function showButton() {
    $($('#stream_connect_button').parent().parent().parent().prev().children('.tab').get(3)).attr('aria-hidden', 'true');
    $($('#stream_connect_button').parent().parent().parent().prev().children('.tab').get(1)).trigger('click');
    $('#stream_connect_button').show();
  }

  function updateState(rfb, state, oldstate, msg) {
    var s, sb, cad, level;
    s = $D('noVNC_status');
    sb = $D('noVNC_status_bar');
    cad = $D('sendCtrlAltDelButton');
    switch (state) {
      case 'failed':       level = "error";  break;
      case 'fatal':        level = "error";  break;
      case 'normal':       level = "normal"; break;
      case 'disconnected':
        level = "normal";
        showButton();
        break;
      case 'loaded':       level = "normal"; break;
      default:             level = "warn";   break;
    }
    if (state === "normal") {
      cad.disabled = false;
    } else {
      cad.disabled = true;
      xvpInit(0);
    }
    if (typeof(msg) !== 'undefined') {
      sb.setAttribute("class", "noVNC_status_" + level);
      s.innerHTML = msg;
    }
  }

  function xvpInit(ver) {
    var xvpbuttons;
    xvpbuttons = $D('noVNC_xvp_buttons');
    if (ver >= 1) {
      xvpbuttons.style.display = 'inline';
    } else {
      xvpbuttons.style.display = 'none';
    }
  }

  window.onscriptsload = function () {
    $('#pointerLockButton').on('click', function(event) {
      var canvas = $('#noVNC_canvas')[0];
      canvas.requestPointerLock = canvas.requestPointerLock ||
                                  canvas.mozRequestPointerLock ||
                                  canvas.webkitRequestPointerLock;

      canvas.requestPointerLock();
    });
    $('#stream_connect_button').on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      $(this).hide();

      window.onresize = function () {
        // When the window has been resized, wait until the size remains
        // the same for 0.5 seconds before sending the request for changing
        // the resolution of the session
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function(){
          UIresize();
        }, 500);
      };

      var host, port, password, path, token;
      $D('sendCtrlAltDelButton').style.display = "inline";
      $D('sendCtrlAltDelButton').onclick = sendCtrlAltDel;
      $D('xvpShutdownButton').onclick = xvpShutdown;
      $D('xvpRebootButton').onclick = xvpReboot;
      $D('xvpResetButton').onclick = xvpReset;

      // No logging
      WebUtil.init_logging('none');

      // By default, use the host and port of server that served this file
      host = WebUtil.getQueryVar('host', window.location.hostname);
      port = WebUtil.getQueryVar('port', window.location.port);
      var ssl = (window.location.protocol.substring(0,5) == 'https');

      // if port == 80 (or 443) then it won't be present and should be
      // set manually
      if (!port) {
        if (window.location.protocol.substring(0,5) == 'https') {
          port = 443;
        }
        else if (window.location.protocol.substring(0,4) == 'http') {
          port = 80;
        }
      }

      token = null

      password = '';
      path = '/vnc/';

      var hostPort = $(this).data('port');
      path = path + hostPort;

      if ((!host) || (!port)) {
        updateState(null, 'fatal', null, 'Must specify host and port in URL');
        return;
      }
      try {
        rfb = new RFB({
          'target':        $D('noVNC_canvas'),
          'encrypt':       ssl,
          'repeaterID':    '',
          'true_color':    true,
          'local_cursor':  true,
          'shared':        true,
          'view_only':     false,
          'onUpdateState': updateState,
          'onXvpInit':     xvpInit,
          'onFBUComplete': FBUComplete,
          'onPasswordRequired':  passwordRequired
        });
      } catch (exc) {
        updateState(null, 'fatal', null, 'Unable to create RFB client -- ' + exc);
        return; // don't continue trying to connect
      }
      rfb.connect(host, port, password, path);
      rfb.get_keyboard().set_focused(false);

      $('html').click(function() {
        rfb.get_keyboard().set_focused(false);
      });

      $('#noVNC_canvas').click(function(event){
        event.stopPropagation();
        rfb.get_keyboard().set_focused(true);
      });
    });
  };
});
