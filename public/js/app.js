/*
 * This is the main module for OCCAM's front end infrastructure. This file will
 * initialize the different modules and initiate some interactive elements on
 * the page.
 */

// The main OCCAM container
window.Occam = {};

// Flags
window.Occam.DEBUG = true;

// On-Load Code:
$(function() {
  var Occam = window.Occam;

  initOccamCard(Occam);
  initOccamObject(Occam);
  initOccamSelector(Occam);
  initOccamDirectory(Occam);
  initOccamAutoComplete(Occam);
  initOccamValidator(Occam);
  initOccamTabs(Occam);
  initOccamConfiguration(Occam);
  initOccamConfigurator(Occam);
  initOccamDataViewer(Occam);
  initOccamRunViewer(Occam);
  initOccamRunTopbar(Occam);
  initOccamVT100(Occam);
  initOccamWebSocket(Occam);
  initOccamTerminal(Occam);
  initOccamPaper(Occam);
  initOccamWorkflow(Occam);
});

// Autocomplete for experiment tags
$(function(){
  var tag_cache = {};
  $('input.tagged').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input.tagged.autocomplete').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input.tagged.autocomplete').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  $('.configuration-group > li > .recipe > select').on('change', function(e) {
    var selected = $(this).children(':selected');
    var ul_group = $(this).parent().parent().children('ul.configuration-group');
    var expand = $(this).parent().parent().children('h2').children('span.expand');
    var nesting = ul_group.data('nesting');

    // Expand group
    if (!expand.hasClass('shown')) {
      expand.trigger('click');
    }

    // Ask for the config options
    jQuery.getJSON(selected.data('template'), function(data, status, xhr) {
      // Set all input fields
      $.each(data, function(key, value) {
        code = $.base64.encode(key);
        // Replace + / with - _
        code = code.replace('+', '-').replace('/', '_');
        // Remove padding
        while (code.charAt(code.length-1) == '=') {
          code = code.slice(0, code.length-1);
        }
        code = nesting + '[' + code + ']';

        $('*[name="'+code+'"]').each(function(e) {
          $(this).val(value);
        });
      });
    });
  });

  // Help bubble expansion
  $('a.help-bubble').on('click', function(event) {
    // Discover the help bubble on the page and toggle it
    var bubble_div = $(this).parent().next();

    if (bubble_div.attr('aria-hidden') == "true") {
      bubble_div.attr('aria-hidden', 'false').slideDown(150);
    }
    else {
      bubble_div.slideUp(150, function() {
        bubble_div.attr('aria-hidden', 'true');
      });
    }
    event.stopPropagation();
    event.preventDefault();
  });

  // Add 'delete' button to help bubbles
  $('.help').append("<div class='delete'></div>");

  // Help 'delete' button should close the help bubble
  $('.help .delete').on('click', function(event) {
    $(this).parent().prev().children('a.help-bubble').trigger('click');
  })
  // Help 'delete' button animation
  .on('mouseenter', function(event) {
    $(this).animate({
      "background-color": "hsl(0, 50%, 50%)",
      "border-color":     "hsl(0, 20%, 50%)"
    }, 200);
  })
  .on('mouseout', function(event) {
    $(this).stop(true).animate({
      "background-color": "hsl(0, 50%, 80%)",
      "border-color":     "hsl(0, 20%, 90%)"
    }, 200);
  });

  // add prettyprint class to all <pre><code></code></pre> blocks
  var prettify = false;
  $("pre code").parent().each(function() {
    $(this).addClass('prettyprint');
    prettify = true;
  });

  // if code blocks were found, bring in the prettifier ...
  if ( prettify ) {
    $.getScript("/js/prettify.js", function() { prettyPrint() });
  }
});
